package cz.cvut.k36.omo.factory.factory;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.mockito.Mockito.*;

public class FactoryUIStandardInOutTest {

    // for checking standard output
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @ParameterizedTest(name = "Valid next tact command: {0} tacts, postponing: {1}")
    @CsvSource({"10, true", "5, false"})
    public void run_validNextTactCommandGiven_correctNumberOfFactoryCalls(int tacts, boolean postpone) {
        // ARRANGE:
        // mocked Factory class
        Factory mockedFactory = mock(Factory.class);
        // creates a command
        String command = "next " + tacts;
        // adds postponing request
        if (postpone) {
            command += " postpone";
        }
        // creates a wrapper
        TestFactoryUI factoryUI = new TestFactoryUI(mockedFactory);
        // expected counts of calls
        int expectedNonPostponeCalls = postpone ? 0 : tacts;
        int expectedPostponeCalls = postpone ? tacts : 0;

        // ACT:
        factoryUI.addCommand(command);
        factoryUI.run();

        // ASSERT:
        verify(mockedFactory, times(expectedNonPostponeCalls)).nextTact(false);
        verify(mockedFactory, times(expectedPostponeCalls)).nextTact(true);
    }

    @ParameterizedTest(name = "Invalid next tact command: {0} tacts, postponing: {1}")
    @CsvSource({"-1, true", "0, false"})
    public void run_invalidNextTactCommandGiven_noCrashNoFactoryCallsErrorQuotePrinted(int tacts, boolean postpone) {
        // ARRANGE:
        // mocked Factory class
        Factory mockedFactory = mock(Factory.class);
        // creates a command
        String command = "next " + tacts;
        // adds postponing request
        if (postpone) {
            command += " postpone";
        }
        // creates a wrapper
        TestFactoryUI factoryUI = new TestFactoryUI(mockedFactory);
        // expected results
        int expectedNonPostponeCalls = 0;
        int expectedPostponeCalls = 0;
        String expectedStdout = "Invalid command, please check it and type again." + System.lineSeparator();
        // sets controlled standard output
        System.setOut(new PrintStream(outContent));

        // ACT:
        factoryUI.addCommand(command);
        factoryUI.run();

        // ASSERT:
        verify(mockedFactory, times(expectedNonPostponeCalls)).nextTact(false);
        verify(mockedFactory, times(expectedPostponeCalls)).nextTact(true);
        Assertions.assertEquals(expectedStdout, outContent.toString());

        // CLEAN:
        System.setOut(originalOut);
    }
}