package cz.cvut.k36.omo.factory.product;

import cz.cvut.k36.omo.factory.production.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

    private static Stream<Arguments> provideInputForBeProcessedBy(){
        return Stream.of(Arguments.of(ProductType.CAR ,new Machine(MachineType.PRESS_SHOP), true),
                        Arguments.of(ProductType.CAR, new Robot(RobotType.COLORING), false),
                        Arguments.of(ProductType.MOTORBIKE, new Machine(MachineType.ENGINE_PLANT), true),
                        Arguments.of(ProductType.CARAVAN, new Robot(RobotType.WHEEL_MOUNTING), false));
    }

    @ParameterizedTest
    @MethodSource("provideInputForBeProcessedBy")
    public void beProcessedBy_workplace_expectedBool(ProductType productType, Workplace workplace, boolean expectedResult){
        //arrange
        Product product = new Product(productType);

        //act
        boolean actualResult = product.beProcessedBy(workplace);

        //assert
        assertEquals(expectedResult, actualResult);
    }
}