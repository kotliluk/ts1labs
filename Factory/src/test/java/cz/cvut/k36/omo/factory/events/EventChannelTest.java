package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Priority;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class EventChannelTest {

    /**
     * Testing extension of Task for EventChannel.
     */
    private class TestTask extends Task {

        protected TestTask(EventChannelAccessible sender, Class<?> recipient) {
            super(Priority.LOW, sender, recipient);
        }
    }

    /**
     * Testing extension of Response for EventChannel.
     */
    private class TestResponse extends Response {

        protected TestResponse(EventChannelAccessible sender, Task previousTask) {
            super(sender, previousTask);
        }
    }

    /**
     * Testing access to the EventChannel (two types to define different sender and receiver).
     */
    private class TestingAccessOne implements EventChannelAccessible {

        @Override
        public void processTask(Task task) {
            // does nothing
        }

        @Override
        public void receiveResponse(Response response) {
            // does nothing
        }
    }

    /**
     * Testing access to the EventChannel (two types to define different sender and receiver).
     */
    private class TestingAccessTwo implements EventChannelAccessible {

        @Override
        public void processTask(Task task) {
            // does nothing
        }

        @Override
        public void receiveResponse(Response response) {
            // does nothing
        }
    }

    @Test
    public void addTask_validTaskGiven_sentToTheExpectedReceiver() {
        // arrange
        EventChannel eventChannel = new EventChannel();
        // creates communication participants
        TestingAccessOne sender = mock(TestingAccessOne.class);
        TestingAccessTwo receiver = mock(TestingAccessTwo.class);
        // adds sender and receiver to the event channel
        eventChannel.attach(sender);
        eventChannel.attach(receiver);
        // creates a task to be sent
        TestTask task = new TestTask(sender, receiver.getClass());

        // act
        eventChannel.addTask(task);

        // assert that the receiver received given task once
        verify(receiver, times(1)).processTask(task);
    }

    @Test
    public void addResponse_validResponseGiven_sentToTheExpectedParticipant() {
        // arrange
        EventChannel eventChannel = new EventChannel();
        // creates communication participants
        TestingAccessOne taskSenderResponseReceiver = mock(TestingAccessOne.class);
        TestingAccessTwo taskReceiverResponseSender = mock(TestingAccessTwo.class);
        // adds sender and receiver to the event channel
        eventChannel.attach(taskSenderResponseReceiver);
        eventChannel.attach(taskReceiverResponseSender);
        // creates an original task
        TestTask task = new TestTask(taskSenderResponseReceiver, taskReceiverResponseSender.getClass());
        // creates a response to a task
        TestResponse response = new TestResponse(taskReceiverResponseSender, task);

        // act
        eventChannel.addResponse(response);

        // assert that the task-sender received given response once
        verify(taskSenderResponseReceiver, times(1)).receiveResponse(response);
    }
}
