package cz.cvut.k36.omo.factory.archive;

import cz.cvut.k36.omo.factory.factory.Factory;
import cz.cvut.k36.omo.factory.production.*;
import cz.cvut.k36.omo.factory.supply.ResourceType;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Factory.class })
public class ArchiveTest {

    @Test
    public void consumptionReport_2TactWork_reportStringFileWithReportCreated() {
        //arrange
        Archive mockedArchive = mock(Archive.class);
        TestableConsumptionArchive testableConsumptionArchive = new TestableConsumptionArchive();
        // mocked to act like first workplaces made (we do not know how many workplaces was created before)
        Machine machine = mock(Machine.class);
        when(machine.toString()).thenReturn("Press shop with ID 1");
        Robot robot = mock(Robot.class);
        when(robot.toString()).thenReturn("Coloring robot with ID 2");
        // mocked Factory to return tact 0 for now
        try {
            PowerMockito.mockStatic(Factory.class);
            PowerMockito.doReturn(0).when(Factory.class, "getCurrentTact");
        }
        catch (Exception e) {
            fail(e);
        }
        List<ConsumptionArchive.ResourceConsumption> history = List.of(
                testableConsumptionArchive.createResourceConsumption(machine, ResourceType.ELECTRICITY, 300, 1),
                testableConsumptionArchive.createResourceConsumption(robot, ResourceType.ELECTRICITY, 150, 1),
                testableConsumptionArchive.createResourceConsumption(machine, ResourceType.ELECTRICITY, 900, 2),
                testableConsumptionArchive.createResourceConsumption(robot,ResourceType.MONEY, 1850, 2),
                testableConsumptionArchive.createResourceConsumption(robot, ResourceType.ELECTRICITY, 500, 2)
        );
        testableConsumptionArchive.addHistory(history);
        when(mockedArchive.getSpecialArchive(ArchiveType.CONSUMPTION)).thenReturn(testableConsumptionArchive);
        when(mockedArchive.consumptionReport()).thenCallRealMethod();
        String expected = "Consumption report was saved into file: consumption_report_tacts-all_generated-0.txt";
        String expectedContent = "Consumption of resources in all tacts."+'\n'
                +"Total use of electricity: 1850"+'\n'
                +"Total use of oil: 0"+'\n'
                +"Individual usages of resources:"+'\n'
                +"Tact 1: Press shop with ID 1 consumed 300 of ELECTRICITY"+'\n'
                +"Tact 1: Coloring robot with ID 2 consumed 150 of ELECTRICITY"+'\n'
                +"Tact 2: Press shop with ID 1 consumed 900 of ELECTRICITY"+'\n'
                +"Tact 2: Coloring robot with ID 2 consumed 1850 of MONEY"+'\n'
                +"Tact 2: Coloring robot with ID 2 consumed 500 of ELECTRICITY"+'\n';

        //act
        String result = mockedArchive.consumptionReport();
        String fileContent = null;
        try {
            fileContent = new String(Files.readAllBytes(Paths.get("consumption_report_tacts-all_generated-0.txt")));
        } catch (IOException e) {
            fail(e);
        }

        //assert
        assertEquals(expected,result);
        assertEquals(expectedContent, fileContent);
    }

    class TestableConsumptionArchive extends ConsumptionArchive{
        ResourceConsumption createResourceConsumption(Workplace wp, ResourceType rt, int material, int tact) {
            return new ResourceConsumption(wp,rt,material,tact);
        }

        void addHistory(List<ResourceConsumption> history){
            this.history.addAll(history);
        }
    }

    @After
    public void cleanUp() {
        try {
            Files.delete(Path.of("consumption_report_tacts-all_generated-0.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}