package cz.cvut.k36.omo.factory.worker;

import cz.cvut.k36.omo.factory.product.ProductType;
import cz.cvut.k36.omo.factory.production.Machine;
import cz.cvut.k36.omo.factory.production.MachineType;
import cz.cvut.k36.omo.factory.production.Robot;
import cz.cvut.k36.omo.factory.production.RobotType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class AccountingTest {

    @ParameterizedTest
    @MethodSource("provideInputForPayWorkerWages")
    public void payWorkerWages_params_expectedResult(int workerWage, List<Worker> workerList, int expected){
        //arrange
        Accounting accounting = new Accounting(workerWage);

        //act
        int result = accounting.payWorkerWages(workerList);

        //assert
        assertEquals(expected,result);
    }

    private static Stream<Arguments> provideInputForPayWorkerWages(){
        Worker w1 = new Worker();
        w1.nextTactOfWorkDone();
        w1.nextTactOfWorkDone();
        w1.nextTactOfWorkDone();

        Worker w2 = new Worker();
        Worker w3 = new Worker();
        w3.nextTactOfWorkDone();
        w3.nextTactOfWorkDone();

        Worker w4 = new Worker();
        Worker w5 = new Worker();
        w4.nextTactOfWorkDone();
        w4.nextTactOfWorkDone();
        w5.nextTactOfWorkDone();
        w5.nextTactOfWorkDone();
        return Stream.of(Arguments.of(100, new ArrayList<>(), 0),
                    Arguments.of(1, List.of(w1), 3),
                    Arguments.of(1, List.of(w2,w3),2),
                    Arguments.of(1, List.of(w4,w5), 4)
                );
    }

}