package cz.cvut.k36.omo.factory.factory;

import cz.cvut.k36.omo.factory.contract.Contract;
import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.events.*;
import cz.cvut.k36.omo.factory.product.Product;
import cz.cvut.k36.omo.factory.product.ProductType;
import cz.cvut.k36.omo.factory.production.*;
import cz.cvut.k36.omo.factory.supply.ResourceType;
import cz.cvut.k36.omo.factory.supply.Stock;
import cz.cvut.k36.omo.factory.supply.Store;
import cz.cvut.k36.omo.factory.worker.Accounting;
import cz.cvut.k36.omo.factory.worker.Worker;

import org.junit.*;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProcessTestJUnit4 {

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    private class FactoryEntities {

        private final EventChannel eventChannel;
        private final Stock stock;
        private final Store store;
        private final Accounting accounting;
        private final Production production;
        private final Factory factory;

        public FactoryEntities(EventChannel eventChannel, Stock stock, Store store, Accounting accounting, Production production, Factory factory) {
            this.eventChannel = eventChannel;
            this.stock = stock;
            this.store = store;
            this.accounting = accounting;
            this.production = production;
            this.factory = factory;
        }
    }

    private final EventChannelAccessible testEventAccess = new EventChannelAccessible() {
        @Override
        public void processTask(Task task) { /* does nothing */ }

        @Override
        public void receiveResponse(Response response) { /* does nothing */ }

        @Override
        public String toString() { return "Testing"; }
    };

    // for checking standard output
    private final PrintStream originalOut = System.out;

    public ProcessTestJUnit4() {
        Logger root = Logger.getLogger("");
        root.getHandlers()[0].setLevel(Level.WARNING);
        root.setLevel(Level.WARNING);
    }

    /**
     * Creates a default factory and returns all its parts.
     */
    private FactoryEntities createFactory() {
        // creates event channel
        EventChannel eventChannel = new EventChannel();
        // creates a stock and adds default number of resources
        Stock stock = new Stock(eventChannel);
        // adds 4 workers
        for (int i = 0; i < 4; ++i) {
            eventChannel.addTask(new StockAddWorkerTask(testEventAccess, Priority.NORMAL, new Worker()));
        }
        // adds each robot once
        for (RobotType rt : RobotType.values()) {
            eventChannel.addTask(new StockAddWorkplaceTask(testEventAccess, Priority.NORMAL, new Robot(rt)));
        }
        // adds each machine once
        for (MachineType mt : MachineType.values()) {
            eventChannel.addTask(new StockAddWorkplaceTask(testEventAccess, Priority.NORMAL, new Machine(mt)));
        }
        // adds each resource 1000x
        for (ResourceType rt : ResourceType.values()) {
            eventChannel.addTask(new StockResourceTask(testEventAccess, Priority.NORMAL, rt, StockResourceTask.Action.ADD, 1000));
        }
        // adds money to 100000 in sum
        eventChannel.addTask(new StockResourceTask(testEventAccess, Priority.NORMAL, ResourceType.MONEY, StockResourceTask.Action.ADD, 99000));
        // adds steal to 10000 in sum
        eventChannel.addTask(new StockResourceTask(testEventAccess, Priority.NORMAL, ResourceType.STEAL, StockResourceTask.Action.ADD, 9000));
        // creates a store with default prices
        Map<WorkplaceType, Integer> wtp = new HashMap<>();
        Map<ResourceType, Integer> rtp = new HashMap<>();
        for (WorkplaceType wt : MachineType.values()) {
            wtp.put(wt, wt.getBasePrice());
        }
        for (WorkplaceType wt : RobotType.values()) {
            wtp.put(wt, wt.getBasePrice());
        }
        for (ResourceType rt : ResourceType.values()) {
            rtp.put(rt, rt.getBasePrice());
        }
        Store store = new Store(eventChannel, wtp, rtp);
        // creates accounting, production and factory
        Accounting accounting = new Accounting(10);
        Production production = new Production(eventChannel, accounting);
        Factory factory = new Factory(production, eventChannel);
        // return all entities
        return new FactoryEntities(eventChannel, stock, store, accounting, production, factory);
    }

    /**
     * Creates a factory with no workplaces, no resources and no money and returns all its parts.
     */
    private FactoryEntities createEmptyFactory() {
        // creates event channel
        EventChannel eventChannel = new EventChannel();
        // creates a stock and adds default number of resources
        Stock stock = new Stock(eventChannel);
        // adds 4 workers
        for (int i = 0; i < 4; ++i) {
            eventChannel.addTask(new StockAddWorkerTask(testEventAccess, Priority.NORMAL, new Worker()));
        }
        // adds no robots, no machines and no resources
        // creates a store with default prices
        Map<WorkplaceType, Integer> wtp = new HashMap<>();
        Map<ResourceType, Integer> rtp = new HashMap<>();
        for (WorkplaceType wt : MachineType.values()) {
            wtp.put(wt, wt.getBasePrice());
        }
        for (WorkplaceType wt : RobotType.values()) {
            wtp.put(wt, wt.getBasePrice());
        }
        for (ResourceType rt : ResourceType.values()) {
            rtp.put(rt, rt.getBasePrice());
        }
        Store store = new Store(eventChannel, wtp, rtp);
        // creates accounting, production and factory
        Accounting accounting = new Accounting(10);
        Production production = new Production(eventChannel, accounting);
        Factory factory = new Factory(production, eventChannel);
        // return all entities
        return new FactoryEntities(eventChannel, stock, store, accounting, production, factory);
    }

    /**
     * Creates a factory with no workplaces, no resources and no money and returns all its parts.
     */
    private FactoryEntities createEmptyFactoryWithMoney() {
        // creates event channel
        EventChannel eventChannel = new EventChannel();
        // creates a stock and adds default number of resources
        Stock stock = new Stock(eventChannel);
        // adds 4 workers
        for (int i = 0; i < 4; ++i) {
            eventChannel.addTask(new StockAddWorkerTask(testEventAccess, Priority.NORMAL, new Worker()));
        }
        // adds no robots, no machines and no resources
        // adds 100000 money
        eventChannel.addTask(new StockResourceTask(testEventAccess, Priority.NORMAL, ResourceType.MONEY, StockResourceTask.Action.ADD, 100000));
        // creates a store with default prices
        Map<WorkplaceType, Integer> wtp = new HashMap<>();
        Map<ResourceType, Integer> rtp = new HashMap<>();
        for (WorkplaceType wt : MachineType.values()) {
            wtp.put(wt, wt.getBasePrice());
        }
        for (WorkplaceType wt : RobotType.values()) {
            wtp.put(wt, wt.getBasePrice());
        }
        for (ResourceType rt : ResourceType.values()) {
            rtp.put(rt, rt.getBasePrice());
        }
        Store store = new Store(eventChannel, wtp, rtp);
        // creates accounting, production and factory
        Accounting accounting = new Accounting(10);
        Production production = new Production(eventChannel, accounting);
        Factory factory = new Factory(production, eventChannel);
        // return all entities
        return new FactoryEntities(eventChannel, stock, store, accounting, production, factory);
    }

    @Before
    public void resetTestingStats() {
        Contract.resetTestingStats();
    }

    @After
    public void cleanUp(){ System.setOut(originalOut); }

    @Test
    public void processTest3_contractGivenToAnEmptyFactoryWithNoMoneyBankrupt() {
        // creates a factory hierarchy and gets its entities
        FactoryEntities entities = createEmptyFactory();
        // stdin/stdout communication with factory
        TestFactoryUI factoryInput = new TestFactoryUI(entities.factory);
        // contract information: low priority contract for 10 cars for 500 money
        Priority priority = Priority.LOW;
        ProductType product = ProductType.CAR;
        int price = 600;
        int amount = 1;
        String expectedContractName = "Contract of priority " + priority + " for " + amount + " " + product + "s";

        // command 1: adds a contract
        String command1 = "add contract " + priority + " " + product + " " + price + " " + amount;
        ByteArrayOutputStream stdout1 = new ByteArrayOutputStream();
        String expectedStdout1 = "Given contract was added into the Production as pending. Production will try to start it in next tact." + System.lineSeparator();
        System.setOut(new PrintStream(stdout1));
        factoryInput.addCommand(command1);
        factoryInput.run();

        // asserts
        // check standard output
        assertEquals(expectedStdout1, stdout1.toString());
        // only one contract should exist, it should have given properties and not be started
        assertEquals(Contract.getContractsCount(), 1);
        Contract contract = Contract.getContracts().get(0);
        assertEquals(contract.getPriority(), priority);
        assertEquals(contract.getOrderedType(), product);
        assertEquals(contract.getProductSellingPrice(), price);
        assertEquals(contract.getOrderedCount(), amount);
        assertEquals(contract.getFinishedCount(), 0);

        // command 2: performs next tact - conflict should occur, we solve it by setting first solution
        String command2 = "next";
        String conflictSolution = "1";
        factoryInput.addCommand(command2);
        factoryInput.addCommand(conflictSolution);
        int expectedTacts = 1;
        ByteArrayOutputStream stdout2 = new ByteArrayOutputStream();
        String expectedPart1 = "********** Tact " + expectedTacts + ": **********";
        String expectedPart2 = "There is a Contract request for \"" + expectedContractName
                + "\", which cannot be satisfied automatically. Please choose a solution from:";
        String expectedPart3 = "\t1) Postpone the contract";
        String expectedPart4 = "\t2) Buy new needed workplaces:";
        String expectedPart5 = "Type number of your chosen solution (1 - 2): ";
        System.setOut(new PrintStream(stdout2));
        factoryInput.run();

        // asserts
        // check standard output
        assertTrue(stdout2.toString().contains(expectedPart1));
        assertTrue(stdout2.toString().contains(expectedPart2));
        assertTrue(stdout2.toString().contains(expectedPart3));
        assertTrue(stdout2.toString().contains(expectedPart4));
        assertTrue(stdout2.toString().contains(expectedPart5));
        for (WorkplaceType wt : product.getConveyorWorkplaces()) {
            assertTrue(stdout2.toString().contains(wt.toString()));
        }

        // command 3: next tact conflict occurs again,
        //            we solve it by setting second solution,
        //            factory doesn't have enough money so we choose bankrupt
        factoryInput.addCommand("next");
        factoryInput.addCommand("2");
        factoryInput.addCommand("bankrupt");
        ByteArrayOutputStream stdout3 = new ByteArrayOutputStream();

        String expectedStdout3_Part1 = "********** Tact 2: **********";
        String expectedStdout3_Part2 = "There is a Contract request for \"" + expectedContractName
                + "\", which cannot be satisfied automatically. Please choose a solution from:";
        String expectedStdout3_Part3 = "\t1) Postpone the contract";
        String expectedStdout3_Part4 = "\t2) Buy new needed workplaces:";
        String expectedStdout3_Part5 = "Type number of your chosen solution (1 - 2): ";

        String expectedStdout3_Part6 = "Give at least the needed amount of money to the factory (type ";
        String expectedStdout3_Part7 = " or higner number)\nor let the factory bankrupt and destroy (type \"bankrupt\"): ";

        String expectedStdout3_Part8 = "\nThe factory did not make enough money to survive in our cruel world...\n" +
                "It has survived for 2 tacts. R.I.P. our factory :(" + System.lineSeparator();

        System.setOut(new PrintStream(stdout3));

        exit.expectSystemExitWithStatus(0);
        exit.checkAssertionAfterwards(() -> {
            assertTrue(stdout3.toString().contains(expectedStdout3_Part1));
            assertTrue(stdout3.toString().contains(expectedStdout3_Part2));
            assertTrue(stdout3.toString().contains(expectedStdout3_Part3));
            assertTrue(stdout3.toString().contains(expectedStdout3_Part4));
            assertTrue(stdout3.toString().contains(expectedStdout3_Part5));
            assertTrue(stdout3.toString().contains(expectedStdout3_Part6));
            assertTrue(stdout3.toString().contains(expectedStdout3_Part7));
            assertTrue(stdout3.toString().contains(expectedStdout3_Part8));
        });
        factoryInput.run();
    }

    /**
     * Process test, where contract is given to empty factory, factory gets half of the money in advance, so it is possible to buy
     * all needed resources, but during work will run out.
     * Factory asks for a solution, we will give it additional money, but it will run out again.
     * Then we let the factory bankrupt.
     */
    @Test
    public void processTest7_contractGivenToEmptyFactoryDoTactsAddMoneyToResolveBankruptToResolve(){
        // creates a factory hierarchy and gets its entities
        FactoryEntities entities = createEmptyFactory();
        // stdin/stdout communication with factory
        TestFactoryUI factoryInput = new TestFactoryUI(entities.factory);

        // contract information: normal priority contract for 1000 cars for 546 money
        Priority priority = Priority.NORMAL;
        ProductType product = ProductType.CAR;
        int price = 546;
        int amount = 1000;

        //command 1: add contract
        String command1 = "add contract " + priority + " " + product + " " + price +" "+amount;
        String expectedStdout1 = "Given contract was added into the Production as pending. Production will try to start it in next tact."+System.lineSeparator();
        ByteArrayOutputStream stdout1 = new ByteArrayOutputStream();
        System.setOut(new PrintStream(stdout1));
        factoryInput.addCommand(command1);
        factoryInput.run();
        //asserts
        assertEquals(expectedStdout1, stdout1.toString());

        //command 2: show resources
        String command2 = "show resources";
        String expectedStdout2 = "Factory has " + amount*price/2 + " money, 0 electricity, 0 oil, 0 steal." + System.lineSeparator();
        ByteArrayOutputStream stdout2 = new ByteArrayOutputStream();
        System.setOut(new PrintStream(stdout2));
        factoryInput.addCommand(command2);
        factoryInput.run();
        //asserts
        assertEquals(expectedStdout2, stdout2.toString());

        //command 3: next, "2" to resolve conflict
        String command3_1 = "next";
        String command3_2 = "2";
        String expectedStdout3_1 = "********** Tact 1: **********";
        String expectedStdout3_2 = "There is a Contract request for \"Contract of priority NORMAL for 1000 CARs\", which cannot be satisfied automatically. Please choose a solution from:";
        String expectedStdout3_3 = "1) Postpone the contract";
        String expectedStdout3_4 = "2) Buy new needed workplaces: ";
        String expectedStdout3_5 = "Type number of your chosen solution (1 - 2): ";
        ByteArrayOutputStream stdout3 = new ByteArrayOutputStream();
        System.setOut(new PrintStream(stdout3));
        factoryInput.addCommand(command3_1);
        factoryInput.addCommand(command3_2);
        factoryInput.run();
        //asserts
        assertTrue(stdout3.toString().contains(expectedStdout3_1));
        assertTrue(stdout3.toString().contains(expectedStdout3_2));
        assertTrue(stdout3.toString().contains(expectedStdout3_3));
        assertTrue(stdout3.toString().contains(expectedStdout3_4));
        assertTrue(stdout3.toString().contains(expectedStdout3_5));

        // command 4: performs next 720 tacts
        int numberOfTacts = 720;
        int tactsBeforeFirstProduct = product.getConveyorWorkplaces().length + 1;
        String command4 = "n " + numberOfTacts;
        ByteArrayOutputStream stdout4 = new ByteArrayOutputStream();
        StringBuilder expectedStdout4 = new StringBuilder();
        int productsCreated = Product.existingProducts();
        for (int tact = 2; tact <= numberOfTacts+1; ++tact) {
            expectedStdout4.append("********** Tact ").append(tact).append(": **********").append(System.lineSeparator());
            if (tact <= tactsBeforeFirstProduct) {
                // nothing more
            }
            else if (tact < tactsBeforeFirstProduct + amount) {
                // next car should be created now
                expectedStdout4.append("Product ").append(product).append("-").append(++productsCreated).append(" from Contract of priority ")
                        .append(priority).append(" for ").append(amount).append(" ").append(product).append("s finished").append(System.lineSeparator());
            }
            else if (tact == tactsBeforeFirstProduct + amount) {
                // last car should be created now
                expectedStdout4.append("Product ").append(product).append("-").append(++productsCreated).append(" from Contract of priority ")
                        .append(priority).append(" for ").append(amount).append(" ").append(product).append("s finished").append(System.lineSeparator());
                // contract should be done
                expectedStdout4.append("Contract of priority ").append(priority).append(" for ").append(amount).append(" ")
                        .append(product).append("s finished").append(System.lineSeparator());
            }
            if(tact == 160) {
                expectedStdout4.append("Accounting tact: 6300 payed to employees of the Factory"+System.lineSeparator());
            } else if(tact%160 == 0) {
                expectedStdout4.append("Accounting tact: 6400 payed to employees of the Factory"+System.lineSeparator());
            }
        }
        System.setOut(new PrintStream(stdout4));
        factoryInput.addCommand(command4);
        factoryInput.run();
        // asserts
        assertEquals(expectedStdout4.toString(), stdout4.toString());

        //command 5: next, (add) "5000" money to resolve conflict
        String command5_1 = "next";
        String command5_2 = "5000";
        String expectedStdout5_1 = "The factory needs 2500 of money to work on."+ System.lineSeparator() +
                "Give at least the needed amount of money to the factory (type 2500 or higner number)\n" +
                "or let the factory bankrupt and destroy (type \"bankrupt\"): ";
        String expectedStdout5_2 = "Product CAR-"+(++productsCreated)+" from Contract of priority NORMAL for 1000 CARs finished";
        ByteArrayOutputStream stdout5 = new ByteArrayOutputStream();
        System.setOut(new PrintStream(stdout5));
        factoryInput.addCommand(command5_1);
        factoryInput.addCommand(command5_2);
        factoryInput.run();
        //asserts
        assertTrue(stdout5.toString().contains(expectedStdout5_1));
        assertTrue(stdout5.toString().contains(expectedStdout5_2));

        //command 6: next 10
        String command6_1 = "next 10";
        String command6_2 = "bankrupt";
        ByteArrayOutputStream stdout6 = new ByteArrayOutputStream();
        StringBuilder expectedStdout6 = new StringBuilder();
        for (int tact = 723; tact <= 731; ++tact) {
            expectedStdout6.append("********** Tact ").append(tact).append(": **********").append(System.lineSeparator());
            if (tact <= tactsBeforeFirstProduct) {
                // nothing more
            }
            else if (tact < tactsBeforeFirstProduct + amount) {
                // next car should be created now
                expectedStdout6.append("Product ").append(product).append("-").append(++productsCreated).append(" from Contract of priority ")
                        .append(priority).append(" for ").append(amount).append(" ").append(product).append("s finished").append(System.lineSeparator());
            }
            else if (tact == tactsBeforeFirstProduct + amount) {
                // last car should be created now
                expectedStdout6.append("Product ").append(product).append("-").append(++productsCreated).append(" from Contract of priority ")
                        .append(priority).append(" for ").append(amount).append(" ").append(product).append("s finished").append(System.lineSeparator());
                // contract should be done
                expectedStdout6.append("Contract of priority ").append(priority).append(" for ").append(amount).append(" ")
                        .append(product).append("s finished").append(System.lineSeparator());
            }
        }
        expectedStdout6.append("********** Tact 732: **********"+System.lineSeparator());
        expectedStdout6.append("The factory needs 500 of money to work on." + System.lineSeparator() +
                "Give at least the needed amount of money to the factory (type 500 or higner number)\n" +
                "or let the factory bankrupt and destroy (type \"bankrupt\"): "+System.lineSeparator());
        expectedStdout6.append("\nThe factory did not make enough money to survive in our cruel world...\n" +
                "It has survived for 732 tacts. R.I.P. our factory :(" + System.lineSeparator());
        System.setOut(new PrintStream(stdout6));
        factoryInput.addCommand(command6_1);
        factoryInput.addCommand(command6_2);

        exit.expectSystemExitWithStatus(0);
        exit.checkAssertionAfterwards(() -> {
            assertEquals(expectedStdout6.toString(), stdout6.toString());
        });

        factoryInput.run();
    }
}
