package cz.cvut.k36.omo.factory.production;

import cz.cvut.k36.omo.factory.contract.Contract;
import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.events.EventChannel;
import cz.cvut.k36.omo.factory.product.ProductType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;

public class ConveyorTest {

    private static ProductType[] getProductTypes() {
        return ProductType.values();
    }

    @ParameterizedTest(name = "Valid decomposition of a conveyor for {0} without a contract")
    @MethodSource("getProductTypes")
    public void decompose_validConveyorWithoutContract_allWorkplacesReturnedConveyorDecomposed(ProductType product) {
        // ARRANGE:
        // get workplaces' types needed for the given product
        WorkplaceType[] workplaceTypes = product.getConveyorWorkplaces();
        // creates workplaces for the conveyor
        List<Workplace> workplaces = new ArrayList<>(workplaceTypes.length);
        for (WorkplaceType wt : workplaceTypes) {
            if (wt.isMachineType()) {
                workplaces.add(new Machine((MachineType)wt));
            }
            else {
                workplaces.add(new Robot((RobotType)wt));
            }
        }
        // creates a mocked event channel
        EventChannel eventChannel = mock(EventChannel.class);
        // creates a production (does not need an accounting)
        Production production = new Production(eventChannel, null);
        // creates a initial contract for a conveyor
        Contract contract = new Contract(Priority.NORMAL, product, 1000, 1);
        // creates a conveyor
        Conveyor conveyor = new Conveyor(production, contract, eventChannel, workplaces);
        // removes a contract to make the conveyor ready for a decomposition
        conveyor.removeContract();

        // ACT:
        // decompose the conveyor and receives its workplaces
        List<Workplace> receivedWorkplaces = conveyor.decompose();

        // ASSERT:
        // conveyor should be decomposed now
        Assertions.assertTrue(conveyor.isDecomposed());
        // conveyor should be detached from an event channel
        verify(eventChannel, times(1)).detach(conveyor);
        // received workplaces should be the same as the given in the conveyor construction
        Set<Workplace> givenSet = new HashSet<>(workplaces);
        Set<Workplace> receivedSet = new HashSet<>(receivedWorkplaces);
        Assertions.assertEquals(givenSet, receivedSet);
        // workplaces after decomposition should be set as free
        receivedWorkplaces.forEach((workplace) -> Assertions.assertFalse(workplace.isInConveyor()));
    }

    @ParameterizedTest(name = "Invalid decomposition of a conveyor for {0} with a contract")
    @MethodSource("getProductTypes")
    public void decompose_validConveyorWithContract_contractIsNotDecomposed(ProductType product) {
        // ARRANGE:
        // get workplaces' types needed for the given product
        WorkplaceType[] workplaceTypes = product.getConveyorWorkplaces();
        // creates workplaces for the conveyor
        List<Workplace> workplaces = new ArrayList<>(workplaceTypes.length);
        for (WorkplaceType wt : workplaceTypes) {
            if (wt.isMachineType()) {
                workplaces.add(new Machine((MachineType)wt));
            }
            else {
                workplaces.add(new Robot((RobotType)wt));
            }
        }
        // creates a mocked event channel
        EventChannel eventChannel = mock(EventChannel.class);
        // creates a production (does not need an accounting)
        Production production = new Production(eventChannel, null);
        // creates a initial contract for a conveyor
        Contract contract = new Contract(Priority.NORMAL, product, 1000, 1);
        // creates a conveyor
        Conveyor conveyor = new Conveyor(production, contract, eventChannel, workplaces);

        // ACT:
        // decompose the conveyor and receives its workplaces
        List<Workplace> receivedWorkplaces = conveyor.decompose();

        // ASSERT:
        // conveyor still should not be decomposed
        Assertions.assertFalse(conveyor.isDecomposed());
        // conveyor still should be attached to an event channel
        verify(eventChannel, times(0)).detach(conveyor);
        // conveyor still should have a contract
        Assertions.assertTrue(conveyor.hasContract());
        // no workplace should be received
        Assertions.assertTrue(receivedWorkplaces.isEmpty());
        // given workplaces still should be added to the conveyor
        workplaces.forEach((workplace) -> Assertions.assertEquals(workplace.getConveyor(), conveyor));
    }

}
