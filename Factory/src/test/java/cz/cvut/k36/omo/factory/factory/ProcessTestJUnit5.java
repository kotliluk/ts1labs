package cz.cvut.k36.omo.factory.factory;

import cz.cvut.k36.omo.factory.archive.Archive;
import cz.cvut.k36.omo.factory.contract.Contract;
import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.events.*;
import cz.cvut.k36.omo.factory.product.Product;
import cz.cvut.k36.omo.factory.product.ProductType;
import cz.cvut.k36.omo.factory.production.*;
import cz.cvut.k36.omo.factory.supply.ResourceType;
import cz.cvut.k36.omo.factory.supply.Stock;
import cz.cvut.k36.omo.factory.supply.Store;
import cz.cvut.k36.omo.factory.worker.Accounting;
import cz.cvut.k36.omo.factory.worker.Worker;

import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.fail;

public class ProcessTestJUnit5 {

    private class FactoryEntities {

        private final EventChannel eventChannel;
        private final Stock stock;
        private final Store store;
        private final Accounting accounting;
        private final Production production;
        private final Factory factory;

        public FactoryEntities(EventChannel eventChannel, Stock stock, Store store, Accounting accounting, Production production, Factory factory) {
            this.eventChannel = eventChannel;
            this.stock = stock;
            this.store = store;
            this.accounting = accounting;
            this.production = production;
            this.factory = factory;
        }
    }

    private final EventChannelAccessible testEventAccess = new EventChannelAccessible() {
        @Override
        public void processTask(Task task) { /* does nothing */ }

        @Override
        public void receiveResponse(Response response) { /* does nothing */ }

        @Override
        public String toString() { return "Testing"; }
    };

    // for checking standard output
    private final PrintStream originalOut = System.out;

    public ProcessTestJUnit5() {
        Logger root = Logger.getLogger("");
        root.getHandlers()[0].setLevel(Level.WARNING);
        root.setLevel(Level.WARNING);
    }

    /**
     * Creates a default factory and returns all its parts.
     */
    private FactoryEntities createFactory() {
        // creates event channel
        EventChannel eventChannel = new EventChannel();
        // creates a stock and adds default number of resources
        Stock stock = new Stock(eventChannel);
        // adds 4 workers
        for (int i = 0; i < 4; ++i) {
            eventChannel.addTask(new StockAddWorkerTask(testEventAccess, Priority.NORMAL, new Worker()));
        }
        // adds each robot once
        for (RobotType rt : RobotType.values()) {
            eventChannel.addTask(new StockAddWorkplaceTask(testEventAccess, Priority.NORMAL, new Robot(rt)));
        }
        // adds each machine once
        for (MachineType mt : MachineType.values()) {
            eventChannel.addTask(new StockAddWorkplaceTask(testEventAccess, Priority.NORMAL, new Machine(mt)));
        }
        // adds each resource 1000x
        for (ResourceType rt : ResourceType.values()) {
            eventChannel.addTask(new StockResourceTask(testEventAccess, Priority.NORMAL, rt, StockResourceTask.Action.ADD, 1000));
        }
        // adds money to 100000 in sum
        eventChannel.addTask(new StockResourceTask(testEventAccess, Priority.NORMAL, ResourceType.MONEY, StockResourceTask.Action.ADD, 99000));
        // adds steal to 10000 in sum
        eventChannel.addTask(new StockResourceTask(testEventAccess, Priority.NORMAL, ResourceType.STEAL, StockResourceTask.Action.ADD, 9000));
        // creates a store with default prices
        Map<WorkplaceType, Integer> wtp = new HashMap<>();
        Map<ResourceType, Integer> rtp = new HashMap<>();
        for (WorkplaceType wt : MachineType.values()) {
            wtp.put(wt, wt.getBasePrice());
        }
        for (WorkplaceType wt : RobotType.values()) {
            wtp.put(wt, wt.getBasePrice());
        }
        for (ResourceType rt : ResourceType.values()) {
            rtp.put(rt, rt.getBasePrice());
        }
        Store store = new Store(eventChannel, wtp, rtp);
        // creates accounting, production and factory
        Accounting accounting = new Accounting(10);
        Production production = new Production(eventChannel, accounting);
        Factory factory = new Factory(production, eventChannel);
        // return all entities
        return new FactoryEntities(eventChannel, stock, store, accounting, production, factory);
    }

    /**
     * Creates a factory with no workplaces, no resources and no money and returns all its parts.
     */
    private FactoryEntities createEmptyFactory() {
        // creates event channel
        EventChannel eventChannel = new EventChannel();
        // creates a stock and adds default number of resources
        Stock stock = new Stock(eventChannel);
        // adds 4 workers
        for (int i = 0; i < 4; ++i) {
            eventChannel.addTask(new StockAddWorkerTask(testEventAccess, Priority.NORMAL, new Worker()));
        }
        // adds no robots, no machines and no resources
        // creates a store with default prices
        Map<WorkplaceType, Integer> wtp = new HashMap<>();
        Map<ResourceType, Integer> rtp = new HashMap<>();
        for (WorkplaceType wt : MachineType.values()) {
            wtp.put(wt, wt.getBasePrice());
        }
        for (WorkplaceType wt : RobotType.values()) {
            wtp.put(wt, wt.getBasePrice());
        }
        for (ResourceType rt : ResourceType.values()) {
            rtp.put(rt, rt.getBasePrice());
        }
        Store store = new Store(eventChannel, wtp, rtp);
        // creates accounting, production and factory
        Accounting accounting = new Accounting(10);
        Production production = new Production(eventChannel, accounting);
        Factory factory = new Factory(production, eventChannel);
        // return all entities
        return new FactoryEntities(eventChannel, stock, store, accounting, production, factory);
    }

    /**
     * Creates a factory with no workplaces, no resources but with money and returns all its parts.
     */
    private FactoryEntities createEmptyFactoryWithMoney() {
        // creates event channel
        EventChannel eventChannel = new EventChannel();
        // creates a stock and adds default number of resources
        Stock stock = new Stock(eventChannel);
        // adds 4 workers
        for (int i = 0; i < 4; ++i) {
            eventChannel.addTask(new StockAddWorkerTask(testEventAccess, Priority.NORMAL, new Worker()));
        }
        // adds no robots, no machines and no resources
        // adds 100000 money
        eventChannel.addTask(new StockResourceTask(testEventAccess, Priority.NORMAL, ResourceType.MONEY, StockResourceTask.Action.ADD, 100000));
        // creates a store with default prices
        Map<WorkplaceType, Integer> wtp = new HashMap<>();
        Map<ResourceType, Integer> rtp = new HashMap<>();
        for (WorkplaceType wt : MachineType.values()) {
            wtp.put(wt, wt.getBasePrice());
        }
        for (WorkplaceType wt : RobotType.values()) {
            wtp.put(wt, wt.getBasePrice());
        }
        for (ResourceType rt : ResourceType.values()) {
            rtp.put(rt, rt.getBasePrice());
        }
        Store store = new Store(eventChannel, wtp, rtp);
        // creates accounting, production and factory
        Accounting accounting = new Accounting(10);
        Production production = new Production(eventChannel, accounting);
        Factory factory = new Factory(production, eventChannel);
        // return all entities
        return new FactoryEntities(eventChannel, stock, store, accounting, production, factory);
    }

    @BeforeEach
    public void resetTestingStats() {
        Contract.resetTestingStats();
        Archive.resetArchivesForTesting();
    }

    @AfterEach
    public void cleanUp(){ System.setOut(originalOut); }

    @Test
    public void processTest1_contractGivenToFactoryWithEnoughWorkplaces() {
        // creates a factory hierarchy and gets its entities
        FactoryEntities entities = createFactory();
        // stdin/stdout communication with factory
        TestFactoryUI factoryInput = new TestFactoryUI(entities.factory);

        // contract information: low priority contract for 10 cars for 500 money
        Priority priority = Priority.LOW;
        ProductType product = ProductType.CAR;
        int price = 1000;
        int amount = 10;

        // command 1: adds a contract
        String command1 = "add contract " + priority + " " + product + " " + price + " " + amount;
        ByteArrayOutputStream stdout1 = new ByteArrayOutputStream();
        String expectedStdout1 = "Given contract was added into the Production as pending. Production will try to start it in next tact." + System.lineSeparator();
        System.setOut(new PrintStream(stdout1));
        factoryInput.addCommand(command1);
        factoryInput.run();
        // asserts
        Assertions.assertEquals(expectedStdout1, stdout1.toString());

        // command 2: performs next 20 tacts
        int numberOfTacts = 20;
        int tactsBeforeFirstProduct = product.getConveyorWorkplaces().length + 1;
        String command2 = "n " + numberOfTacts;
        ByteArrayOutputStream stdout2 = new ByteArrayOutputStream();
        StringBuilder expectedStdout2 = new StringBuilder();
        int productsCreated = Product.existingProducts();
        for (int tact = 1; tact <= numberOfTacts; ++tact) {
            expectedStdout2.append("********** Tact ").append(tact).append(": **********").append(System.lineSeparator());
            if (tact <= tactsBeforeFirstProduct) {
                // nothing more
            }
            else if (tact < tactsBeforeFirstProduct + amount) {
                // next car should be created now
                expectedStdout2.append("Product ").append(product).append("-").append(++productsCreated).append(" from Contract of priority ")
                .append(priority).append(" for ").append(amount).append(" ").append(product).append("s finished").append(System.lineSeparator());
            }
            else if (tact == tactsBeforeFirstProduct + amount) {
                // last car should be created now
                expectedStdout2.append("Product ").append(product).append("-").append(++productsCreated).append(" from Contract of priority ")
                        .append(priority).append(" for ").append(amount).append(" ").append(product).append("s finished").append(System.lineSeparator());
                // contract should be done
                expectedStdout2.append("Contract of priority ").append(priority).append(" for ").append(amount).append(" ")
                        .append(product).append("s finished").append(System.lineSeparator());
            }
        }
        System.setOut(new PrintStream(stdout2));
        factoryInput.addCommand(command2);
        factoryInput.run();
        // asserts
        Assertions.assertEquals(expectedStdout2.toString(), stdout2.toString());
    }

    @Test
    public void processTest2_contractGivenToAnEmptyFactoryWithMoney() {
        // creates a factory hierarchy and gets its entities
        FactoryEntities entities = createEmptyFactoryWithMoney();
        // stdin/stdout communication with factory
        TestFactoryUI factoryInput = new TestFactoryUI(entities.factory);

        // contract information: low priority contract for 10 cars for 500 money
        Priority priority = Priority.LOW;
        ProductType product = ProductType.CAR;
        int price = 1000;
        int amount = 5;
        String expectedContractName = "Contract of priority " + priority + " for " + amount + " " + product + "s";

        // command 1: adds a contract
        String command1 = "add contract " + priority + " " + product + " " + price + " " + amount;
        ByteArrayOutputStream stdout1 = new ByteArrayOutputStream();
        String expectedStdout1 = "Given contract was added into the Production as pending. Production will try to start it in next tact." + System.lineSeparator();
        System.setOut(new PrintStream(stdout1));
        factoryInput.addCommand(command1);
        factoryInput.run();

        // asserts
        // check standard output
        Assertions.assertEquals(expectedStdout1, stdout1.toString());
        // only one contract should exist, it should have given properties and not be started
        Assertions.assertEquals(1, Contract.getContractsCount());
        Contract contract = Contract.getContracts().get(0);
        Assertions.assertEquals(priority, contract.getPriority());
        Assertions.assertEquals(product, contract.getOrderedType());
        Assertions.assertEquals(price, contract.getProductSellingPrice());
        Assertions.assertEquals(amount, contract.getOrderedCount());
        Assertions.assertEquals(0, contract.getFinishedCount());

        // command 2: performs next tact - conflict should occur, we solve it by setting second solution
        String command2 = "next";
        String command3 = "2";
        int expectedTacts = 1;
        ByteArrayOutputStream stdout2 = new ByteArrayOutputStream();
        String expectedPart1 = "********** Tact " + expectedTacts + ": **********";
        String expectedPart2 = "There is a Contract request for \"" + expectedContractName
                + "\", which cannot be satisfied automatically. Please choose a solution from:";
        String expectedPart3 = "\t1) Postpone the contract";
        String expectedPart4 = "\t2) Buy new needed workplaces:";
        String expectedPart5 = "Type number of your chosen solution (1 - 2): ";
        System.setOut(new PrintStream(stdout2));
        factoryInput.addCommand(command2);
        factoryInput.addCommand(command3);
        factoryInput.run();

        // asserts
        // check standard output
        Assertions.assertTrue(stdout2.toString().contains(expectedPart1));
        Assertions.assertTrue(stdout2.toString().contains(expectedPart2));
        Assertions.assertTrue(stdout2.toString().contains(expectedPart3));
        Assertions.assertTrue(stdout2.toString().contains(expectedPart4));
        Assertions.assertTrue(stdout2.toString().contains(expectedPart5));
        for (WorkplaceType wt : product.getConveyorWorkplaces()) {
            Assertions.assertTrue(stdout2.toString().contains(wt.toString()));
        }

        // command 3: performs next 12 tacts
        int previousTacts = expectedTacts;
        int numberOfTacts = 12;
        expectedTacts += numberOfTacts;
        int tactsBeforeFirstProduct = previousTacts + product.getConveyorWorkplaces().length;
        String command4 = "n " + numberOfTacts;
        ByteArrayOutputStream stdout3 = new ByteArrayOutputStream();
        StringBuilder expectedStdout3 = new StringBuilder();
        int productsCreated = Product.existingProducts();
        for (int tact = previousTacts + 1; tact <= expectedTacts; ++tact) {
            expectedStdout3.append("********** Tact ").append(tact).append(": **********").append(System.lineSeparator());
            if (tact <= tactsBeforeFirstProduct) {
                // nothing more
            }
            else if (tact < tactsBeforeFirstProduct + amount) {
                // next product should be created now
                expectedStdout3.append("Product ").append(product).append("-").append(++productsCreated).append(" from ")
                        .append(expectedContractName).append(" finished").append(System.lineSeparator());
            }
            else if (tact == tactsBeforeFirstProduct + amount) {
                // last product should be created now
                expectedStdout3.append("Product ").append(product).append("-").append(++productsCreated).append(" from ")
                        .append(expectedContractName).append(" finished").append(System.lineSeparator());
                // contract should be done
                expectedStdout3.append(expectedContractName).append(" finished").append(System.lineSeparator());
            }
        }
        System.setOut(new PrintStream(stdout3));
        factoryInput.addCommand(command4);
        factoryInput.run();

        // asserts
        // check standard output
        Assertions.assertEquals(expectedStdout3.toString(), stdout3.toString());
        // still only one contract should exist, it should be finished now
        Assertions.assertEquals(Contract.getContractsCount(), 1);
        contract = Contract.getContracts().get(0);
        Assertions.assertEquals(contract.getPriority(), priority);
        Assertions.assertEquals(contract.getOrderedType(), product);
        Assertions.assertEquals(contract.getProductSellingPrice(), price);
        Assertions.assertEquals(contract.getOrderedCount(), amount);
        Assertions.assertEquals(contract.getFinishedCount(), amount);
        Assertions.assertTrue(contract.isFinished());
    }

    @Test
    public void processTest4_invalidContract_quitNo_NextTactNothingDone_quitYes() {
        // creates a factory hierarchy and gets its entities
        FactoryEntities entities = createEmptyFactoryWithMoney();
        // stdin/stdout communication with factory
        TestFactoryUI factoryInput = new TestFactoryUI(entities.factory);

        // contract information: low priority contract for 5 cars for 0 money
        Priority priority = Priority.LOW;
        ProductType product = ProductType.CAR;
        int price = 0;
        int amount = 5;

        // command 1: adds an invalid contract
        String command1 = "add contract " + priority + " " + product + " " + price + " " + amount;
        ByteArrayOutputStream stdout1 = new ByteArrayOutputStream();
        String expectedStdout1 = "Given contract's price must be positive."+ System.lineSeparator() +
                "Invalid command, please check it and type again." + System.lineSeparator();
        System.setOut(new PrintStream(stdout1));
        factoryInput.addCommand(command1);
        factoryInput.run();

        // asserts
        // check standard output
        Assertions.assertEquals(expectedStdout1, stdout1.toString());

        //command 2: quit, no
        String command2 = "quit";
        String command2_2 = "no";
        ByteArrayOutputStream stdout2 = new ByteArrayOutputStream();
        String expectedStdout2 = "Type \"yes\" to confirm exiting the factory: ";
        System.setOut(new PrintStream(stdout2));
        factoryInput.addCommand(command2);
        factoryInput.addCommand(command2_2);
        factoryInput.run();

        // asserts
        // check standard output
        Assertions.assertEquals(expectedStdout2, stdout2.toString());

        //command 3: next
        String command3 = "next";
        ByteArrayOutputStream stdout3 = new ByteArrayOutputStream();
        String expectedStdout3 = "********** Tact 1: **********" + System.lineSeparator();
        System.setOut(new PrintStream(stdout3));
        factoryInput.addCommand(command3);
        factoryInput.run();

        // asserts
        // check standard output
        Assertions.assertEquals(expectedStdout3, stdout3.toString());

        //command 4: quit, yes
        String command4 = "quit";
        String command4_2 = "yes";
        ByteArrayOutputStream stdout4 = new ByteArrayOutputStream();
        String expectedStdout4 = "Type \"yes\" to confirm exiting the factory: " + "Factory exited after 1 tacts."+System.lineSeparator();
        System.setOut(new PrintStream(stdout4));
        factoryInput.addCommand(command4);
        factoryInput.addCommand(command4_2);
        factoryInput.run();

        // asserts
        // check standard output
        Assertions.assertEquals(expectedStdout4, stdout4.toString());
    }

    @Test
    public void processTest5_emptyFactory_showResources_addMoneyAddContractBuyWorkplacesMakeProducts() {
        // creates a factory hierarchy and gets its entities
        FactoryEntities entities = createEmptyFactory();
        // stdin/stdout communication with factory
        TestFactoryUI factoryInput = new TestFactoryUI(entities.factory);

        //command 1: show resources
        String command1 = "show resources";
        ByteArrayOutputStream stdout1 = new ByteArrayOutputStream();
        String expectedStdout1 = "Factory has 0 money, 0 electricity, 0 oil, 0 steal." + System.lineSeparator();
        System.setOut(new PrintStream(stdout1));
        factoryInput.addCommand(command1);
        factoryInput.run();

        // asserts
        // check standard output
        Assertions.assertEquals(expectedStdout1, stdout1.toString());

        //command 2: add money
        String command2 = "add money 100000";
        ByteArrayOutputStream stdout2 = new ByteArrayOutputStream();
        String expectedStdout2 = "100000 money added" + System.lineSeparator();
        System.setOut(new PrintStream(stdout2));
        factoryInput.addCommand(command2);
        factoryInput.run();

        // asserts
        // check standard output
        Assertions.assertEquals(expectedStdout2, stdout2.toString());

        //command 3: show resources
        String command3 = "show resources";
        ByteArrayOutputStream stdout3 = new ByteArrayOutputStream();
        String expectedStdout3 = "Factory has 100000 money, 0 electricity, 0 oil, 0 steal." + System.lineSeparator();
        System.setOut(new PrintStream(stdout3));
        factoryInput.addCommand(command3);
        factoryInput.run();

        // asserts
        // check standard output
        Assertions.assertEquals(expectedStdout3, stdout3.toString());

        //command 4: add contract
        // contract information: normal priority contract for 20 caravans for 1500 money
        Priority priority = Priority.NORMAL;
        ProductType product = ProductType.CARAVAN;
        int price = 1500;
        int amount = 20;
        String command4 = "add contract " + priority + " " + product + " " + price + " " + amount;
        ByteArrayOutputStream stdout4 = new ByteArrayOutputStream();
        String expectedStdout4 = "Given contract was added into the Production as pending. Production will try to start it in next tact." + System.lineSeparator();
        System.setOut(new PrintStream(stdout4));
        factoryInput.addCommand(command4);
        factoryInput.run();

        // asserts
        // check standard output
        Assertions.assertEquals(expectedStdout4, stdout4.toString());

        //command 5: next, resolve conflict with solution 2
        String command5_1 = "next";
        String command5_2 = "2";
        ByteArrayOutputStream stdout5 = new ByteArrayOutputStream();
        String expectedStdout5_part1 = "********** Tact 1: **********";
        String expectedStdout5_part2 = "There is a Contract request for \"Contract of priority NORMAL for 20 CARAVANs\", which cannot be satisfied automatically. Please choose a solution from:";
        String expectedStdout5_part3 = "\t1) Postpone the contract";
        String expectedStdout5_part4 = "\t2) Buy new needed workplaces:";
        String expectedStdout5_part5 = "Type number of your chosen solution (1 - 2): ";
        System.setOut(new PrintStream(stdout5));
        factoryInput.addCommand(command5_1);
        factoryInput.addCommand(command5_2);
        factoryInput.run();

        // asserts
        // check standard output
        Assertions.assertTrue(stdout5.toString().contains(expectedStdout5_part1));
        Assertions.assertTrue(stdout5.toString().contains(expectedStdout5_part2));
        Assertions.assertTrue(stdout5.toString().contains(expectedStdout5_part3));
        Assertions.assertTrue(stdout5.toString().contains(expectedStdout5_part4));
        Assertions.assertTrue(stdout5.toString().contains(expectedStdout5_part5));

        //command 6: perform next 28 tacts (to tact 29)
        int numberOfTacts = 28;
        int tactsBeforeFirstProduct = product.getConveyorWorkplaces().length + 1;
        String command6 = "n " + numberOfTacts;
        ByteArrayOutputStream stdout6 = new ByteArrayOutputStream();
        StringBuilder expectedStdout6 = new StringBuilder();
        int productsCreated = Product.existingProducts();
        for (int tact = 2; tact <= numberOfTacts + 1; ++tact) {
            expectedStdout6.append("********** Tact ").append(tact).append(": **********").append(System.lineSeparator());
            if (tact <= tactsBeforeFirstProduct) {
                // nothing more
            }
            else if (tact < tactsBeforeFirstProduct + amount) {
                // next car should be created now
                expectedStdout6.append("Product ").append(product).append("-").append(++productsCreated).append(" from Contract of priority ")
                        .append(priority).append(" for ").append(amount).append(" ").append(product).append("s finished").append(System.lineSeparator());
            }
            else if (tact == tactsBeforeFirstProduct + amount) {
                // last car should be created now
                expectedStdout6.append("Product ").append(product).append("-").append(++productsCreated).append(" from Contract of priority ")
                        .append(priority).append(" for ").append(amount).append(" ").append(product).append("s finished").append(System.lineSeparator());
                // contract should be done
                expectedStdout6.append("Contract of priority ").append(priority).append(" for ").append(amount).append(" ")
                        .append(product).append("s finished").append(System.lineSeparator());
            }
        }
        System.setOut(new PrintStream(stdout6));
        factoryInput.addCommand(command6);
        factoryInput.run();

        // asserts
        // check standard output
        Assertions.assertEquals(stdout6.toString(), expectedStdout6.toString());
    }

    @Test
    public void processTest6_giveContractToFactory_doWork_reports() {
        // creates a factory hierarchy and gets its entities
        FactoryEntities entities = createFactory();
        // stdin/stdout communication with factory
        TestFactoryUI factoryInput = new TestFactoryUI(entities.factory);

        //command 1: add contract
        // contract information: low priority contract for 10 cars for 1000 money
        Priority priority = Priority.LOW;
        ProductType product = ProductType.CAR;
        int price = 1000;
        int amount = 10;
        String command1 = "add contract " + priority + " " + product + " " + price + " " + amount;
        ByteArrayOutputStream stdout1 = new ByteArrayOutputStream();
        String expectedStdout1 = "Given contract was added into the Production as pending. Production will try to start it in next tact." + System.lineSeparator();
        System.setOut(new PrintStream(stdout1));
        factoryInput.addCommand(command1);
        factoryInput.run();
        // asserts
        Assertions.assertEquals(expectedStdout1, stdout1.toString());

        //command 2: next
        String command2 = "next";
        ByteArrayOutputStream stdout2 = new ByteArrayOutputStream();
        String expectedStdout2 = "********** Tact 1: **********" + System.lineSeparator();
        System.setOut(new PrintStream(stdout2));
        factoryInput.addCommand(command2);
        factoryInput.run();
        // asserts
        Assertions.assertEquals(expectedStdout2, stdout2.toString());

        //command 3: report configuration
        String command3 = "report configuration";
        ByteArrayOutputStream stdout3 = new ByteArrayOutputStream();
        String expectedStdout3 = "Configuration report was saved into file: config_report_tacts-all_generated-1.txt" + System.lineSeparator();
        System.setOut(new PrintStream(stdout3));
        factoryInput.addCommand(command3);
        factoryInput.run();

        String expectedReportContent_1 = "Configuration report for all tacts. All conveyors are counted.";
        String expectedReportContent_2 = "- Conveyors, which are still available:";
        String expectedReportContent_3 = " for CAR (created in tact 1) contains:";
        String expectedReportContent_4 = "Press shop with ID";
        String expectedReportContent_5 = "Engine plant with ID";
        String expectedReportContent_6 = "Assembly shop with ID";
        String expectedReportContent_7 = "Wheel mounting robot with ID";
        String expectedReportContent_8 = "Interior setting with ID";
        String expectedReportContent_9 = "Coloring robot with ID";
        String expectedReportContent_10 = "- Conveyors, which has been decomposed:";
        String expectedReportContent_11 = "No such conveyor exists.";
        String reportFileContent = null;
        try {
            reportFileContent = new String(Files.readAllBytes(Paths.get("config_report_tacts-all_generated-1.txt")));
        } catch (IOException e) {
            fail(e);
        }
        // asserts
        Assertions.assertEquals(expectedStdout3, stdout3.toString());
        Assertions.assertTrue(reportFileContent.contains(expectedReportContent_1));
        Assertions.assertTrue(reportFileContent.contains(expectedReportContent_2));
        Assertions.assertTrue(reportFileContent.contains(expectedReportContent_3));
        Assertions.assertTrue(reportFileContent.contains(expectedReportContent_4));
        Assertions.assertTrue(reportFileContent.contains(expectedReportContent_5));
        Assertions.assertTrue(reportFileContent.contains(expectedReportContent_6));
        Assertions.assertTrue(reportFileContent.contains(expectedReportContent_7));
        Assertions.assertTrue(reportFileContent.contains(expectedReportContent_8));
        Assertions.assertTrue(reportFileContent.contains(expectedReportContent_9));
        Assertions.assertTrue(reportFileContent.contains(expectedReportContent_10));
        Assertions.assertTrue(reportFileContent.contains(expectedReportContent_11));
        //delete files
        try {
            Files.delete(Paths.get("config_report_tacts-all_generated-1.txt"));
        } catch (IOException e) {
            fail(e);
        }

        // command 4: next 20 tacts
        int numberOfTacts = 20;
        int tactsBeforeFirstProduct = product.getConveyorWorkplaces().length + 1;
        String command4 = "n " + numberOfTacts;
        ByteArrayOutputStream stdout4 = new ByteArrayOutputStream();
        StringBuilder expectedStdout4 = new StringBuilder();
        int productsCreated = Product.existingProducts();
        for (int tact = 2; tact <= numberOfTacts+1; ++tact) {
            expectedStdout4.append("********** Tact ").append(tact).append(": **********").append(System.lineSeparator());
            if (tact <= tactsBeforeFirstProduct) {
                // nothing more
            }
            else if (tact < tactsBeforeFirstProduct + amount) {
                // next car should be created now
                expectedStdout4.append("Product ").append(product).append("-").append(++productsCreated).append(" from Contract of priority ")
                        .append(priority).append(" for ").append(amount).append(" ").append(product).append("s finished").append(System.lineSeparator());
            }
            else if (tact == tactsBeforeFirstProduct + amount) {
                // last car should be created now
                expectedStdout4.append("Product ").append(product).append("-").append(++productsCreated).append(" from Contract of priority ")
                        .append(priority).append(" for ").append(amount).append(" ").append(product).append("s finished").append(System.lineSeparator());
                // contract should be done
                expectedStdout4.append("Contract of priority ").append(priority).append(" for ").append(amount).append(" ")
                        .append(product).append("s finished").append(System.lineSeparator());
            }
        }
        System.setOut(new PrintStream(stdout4));
        factoryInput.addCommand(command4);
        factoryInput.run();
        // asserts
        Assertions.assertEquals(expectedStdout4.toString(), stdout4.toString());

        //command 5: report CONSUMPTION
        String command5 = "report consumption";
        ByteArrayOutputStream stdout5 = new ByteArrayOutputStream();
        String expectedStdout5 = "Consumption report was saved into file: consumption_report_tacts-all_generated-21.txt" + System.lineSeparator();
        System.setOut(new PrintStream(stdout5));
        factoryInput.addCommand(command5);
        factoryInput.run();

        String expectedReportContent5_1 = "Consumption of resources in all tacts.";
        String expectedReportContent5_2 = "Total use of electricity: 120";
        String expectedReportContent5_3 = "Total use of oil: 60";
        String fileContent = null;
        try {
            fileContent = new String(Files.readAllBytes(Paths.get("consumption_report_tacts-all_generated-21.txt")));
        } catch (IOException e) {
            fail(e);
        }

        //asserts
        Assertions.assertEquals(expectedStdout5, stdout5.toString());
        Assertions.assertTrue(fileContent.contains(expectedReportContent5_1));
        Assertions.assertTrue(fileContent.contains(expectedReportContent5_2));
        Assertions.assertTrue(fileContent.contains(expectedReportContent5_3));
        //delete files
        try {
            Files.delete(Paths.get("consumption_report_tacts-all_generated-21.txt"));
        } catch (IOException e) {
            fail(e);
        }

        //command 6: report EVENT 17 18 (last tact when something happened)
        String command6 = "report event 17 18";
        ByteArrayOutputStream stdout6 = new ByteArrayOutputStream();
        String expectedStdout6 = "Event report was saved into file: event_report_tacts-17-18_generated-21.txt" + System.lineSeparator();
        System.setOut(new PrintStream(stdout6));
        factoryInput.addCommand(command6);
        factoryInput.run();

        String expectedReportContent6_1 = "Event report for tacts 17 – 18.";
        String expectedReportContent6_2 = "Tact17: MessageToUserTask of priority LOW from Conveyor for product CAR with current contract \"null\" to Factory";
        String expectedReportContent6_3 = "Tact17: MessageToUserTask of priority NORMAL from Production to Factory";
        String expectedReportContent6_4 = "Tact17: StockResourceTask from Production to add 5000 of MONEY";
        String reportContent6 = null;
        try {
            reportContent6 = new String(Files.readAllBytes(Paths.get("event_report_tacts-17-18_generated-21.txt")));
        } catch (IOException e) {
            fail(e);
        }

        //asserts
        Assertions.assertEquals(expectedStdout6, stdout6.toString());
        Assertions.assertTrue(reportContent6.contains(expectedReportContent6_1));
        Assertions.assertTrue(reportContent6.contains(expectedReportContent6_2));
        Assertions.assertTrue(reportContent6.contains(expectedReportContent6_3));
        Assertions.assertTrue(reportContent6.contains(expectedReportContent6_4));
        //delete files
        try {
            Files.delete(Paths.get("event_report_tacts-17-18_generated-21.txt"));
        } catch (IOException e) {
            fail(e);
        }
    }

    @Test
    public void processTest8_givenContractToEmptyFactoryAndAddMoney_postponeWorkThenDoIt(){
        // creates a factory hierarchy and gets its entities
        FactoryEntities entities = createEmptyFactory();
        // stdin/stdout communication with factory
        TestFactoryUI factoryInput = new TestFactoryUI(entities.factory);

        //command 1: add money
        String command1 = "add money 12000";
        String expectedStdout1 = "12000 money added" + System.lineSeparator();
        ByteArrayOutputStream stdout1 = new ByteArrayOutputStream();
        System.setOut(new PrintStream(stdout1));
        factoryInput.addCommand(command1);
        factoryInput.run();
        //asserts
        Assertions.assertEquals(expectedStdout1, stdout1.toString());

        //command 2: show resources (check if the money was really added)
        String command2 = "show resources";
        String expectedStdout2 = "Factory has 12000 money, 0 electricity, 0 oil, 0 steal."+System.lineSeparator();
        ByteArrayOutputStream stdout2 = new ByteArrayOutputStream();
        System.setOut(new PrintStream(stdout2));
        factoryInput.addCommand(command2);
        factoryInput.run();
        //asserts
        Assertions.assertEquals(expectedStdout2, stdout2.toString());

        //command 3: add contract
        Priority priority = Priority.HIGH;
        ProductType product = ProductType.MOTORBIKE;
        int price = 1000;
        int amount = 2;
        String command3 = "add contract " + priority + " " + product + " " + price + " " + amount;
        ByteArrayOutputStream stdout3 = new ByteArrayOutputStream();
        String expectedStdout3 = "Given contract was added into the Production as pending. Production will try to start it in next tact." + System.lineSeparator();
        System.setOut(new PrintStream(stdout3));
        factoryInput.addCommand(command3);
        factoryInput.run();
        //asserts
        Assertions.assertEquals(expectedStdout3, stdout3.toString());

        //command 4: next 5 postpone
        String command4 = "next 5 postpone";
        StringBuilder expectedStdout4 = new StringBuilder();
        for (int tact = 1; tact <= 5; tact++) {
            expectedStdout4.append("********** Tact ").append(tact).append(": **********").append(System.lineSeparator());
            expectedStdout4.append("Contract request for \"Contract of priority HIGH for 2 MOTORBIKEs\" was automatically postponed.").append(System.lineSeparator());
        }
        ByteArrayOutputStream stdout4 = new ByteArrayOutputStream();
        System.setOut(new PrintStream(stdout4));
        factoryInput.addCommand(command4);
        factoryInput.run();
        //asserts
        Assertions.assertEquals(expectedStdout4.toString(), stdout4.toString());

        //command 5: next 6, "2" to resolve conflict
        String command5_1 = "next 6";
        String command5_2 = "2";
        ByteArrayOutputStream stdout5 = new ByteArrayOutputStream();
        int tactsBeforeFirstProduct = product.getConveyorWorkplaces().length + 6;
        int productsCreated = Product.existingProducts();
        String expectedStdout5_1 = "********** Tact 6: **********"+System.lineSeparator()+"There is a Contract request for \"Contract of priority HIGH for 2 MOTORBIKEs\", which cannot be satisfied automatically. Please choose a solution from:"+System.lineSeparator() +
                "\t1) Postpone the contract" + System.lineSeparator() +
                "\t2) Buy new needed workplaces:";
        String expectedStdout5_2 = "Type number of your chosen solution (1 - 2): "+System.lineSeparator();
        StringBuilder expectedStdout5_3 = new StringBuilder();
        for (int tact = 7; tact <= 11; tact++) {
            expectedStdout5_3.append("********** Tact ").append(tact).append(": **********").append(System.lineSeparator());
            if (tact <= tactsBeforeFirstProduct) {
                // nothing more
            }
            else if (tact < tactsBeforeFirstProduct + amount) {
                // next car should be created now
                expectedStdout5_3.append("Product ").append(product).append("-").append(++productsCreated).append(" from Contract of priority ")
                        .append(priority).append(" for ").append(amount).append(" ").append(product).append("s finished").append(System.lineSeparator());
            }
            else if (tact == tactsBeforeFirstProduct + amount) {
                // last car should be created now
                expectedStdout5_3.append("Product ").append(product).append("-").append(++productsCreated).append(" from Contract of priority ")
                        .append(priority).append(" for ").append(amount).append(" ").append(product).append("s finished").append(System.lineSeparator());
                // contract should be done
                expectedStdout5_3.append("Contract of priority ").append(priority).append(" for ").append(amount).append(" ")
                        .append(product).append("s finished").append(System.lineSeparator());
            }
        }

        System.setOut(new PrintStream(stdout5));
        factoryInput.addCommand(command5_1);
        factoryInput.addCommand(command5_2);
        factoryInput.run();
        //asserts
        Assertions.assertTrue(stdout5.toString().contains(expectedStdout5_1));
        Assertions.assertTrue(stdout5.toString().contains(expectedStdout5_2));
        Assertions.assertTrue(stdout5.toString().contains(expectedStdout5_3.toString()));
    }
}
