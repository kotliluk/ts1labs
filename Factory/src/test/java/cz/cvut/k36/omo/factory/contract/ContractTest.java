package cz.cvut.k36.omo.factory.contract;

import cz.cvut.k36.omo.factory.product.Product;
import cz.cvut.k36.omo.factory.product.ProductType;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ContractTest {

    @ParameterizedTest
    @MethodSource("provideInputForAddFinishedProduct")
    public void addFinishedProduct_product_expectedResult(Product product, boolean expected){
        //arrange
        Contract contract = new Contract(Priority.NORMAL, product.getType(), 10, 5);

        //act
        boolean result = contract.addFinishedProduct(product);
        List<Product> list = contract.getProducts();

        //assert
        assertEquals(expected, result);
        assertEquals(expected, list.contains(product));
    }

    private static Stream<Arguments> provideInputForAddFinishedProduct(){

        Product p1 = new Product(ProductType.CAR);

        //mocked product, representing finished product
        Product mockedP2 = mock(Product.class);
        when(mockedP2.getType()).thenReturn(ProductType.CAR);
        when(mockedP2.isDone()).thenReturn(true);
        when(mockedP2.isSold()).thenReturn(false);

        //mocked product, finished but already sold
        Product mockedP3 = mock(Product.class);
        when(mockedP3.getType()).thenReturn(ProductType.CAR);
        when(mockedP3.isDone()).thenReturn(true);
        when(mockedP3.isSold()).thenReturn(true);

        return Stream.of(Arguments.of(p1, false),
                    Arguments.of(mockedP2, true),
                    Arguments.of(mockedP3, false)
                );
    }

}