package cz.cvut.k36.omo.factory.factory;

import cz.cvut.k36.omo.factory.archive.ArchiveType;
import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.product.ProductType;

import java.util.LinkedList;
import java.util.Queue;

public class TestFactoryUI implements FactoryUIWrapper {

    private static final String NEXT_COMMAND_SHORT = "n";
    private static final String NEXT_COMMAND_LONG = "next";
    private static final String HELP_COMMAND = "help";
    private static final String EXIT_COMMAND = "quit";
    private static final String CONTRACT_COMMAND = "add contract";
    private static final String ADD_MONEY_COMMAND = "add money";
    private static final String REPORT_COMMAND = "report";
    private static final String YES_COMMAND = "yes";
    private static final String SHOW_RESOURCES_COMMAND = "show resources";
    private static final String HELP_TEXT = "Use these commands to control the factory (note that commands are NOT case sensitive):\n"
            + "\"n [<NUMBER> [postpone]]\", \"next [<NUMBER> [postpone]]\"\n\tNext NUMBER steps of processing contracts in the factory is done, if NUMBER is not set, "
            + "1 step is done. NUMBER should be a positive integer. If \"postpone\" is set as third parameter, all contract conflicts will be solved by postponing. "
            + "This command ends adding commands in the current tact and starts processing in the factory\n"
            + "\"add contract <PRIORITY> <TYPE> <PRICE> <AMOUNT>\"\n\tA new contract for the factory is declared. PRIORITY should be string"
            + " LOW/NORMAL/HIGH, TYPE should be CAR/MOTORBIKE/TRUCK/BUS/CARAVAN, PRICE and AMOUNT should be positive integer values\n"
            + "\"add money <AMOUNT>\"\n\tFactory receives given amount of money. MONEY should be a positive integer value\n"
            + "\"quit\"\n\tFactory is exited\n"
            + "\"report <TYPE> [<START> <END>]\"\n\tGiven TYPE of report is created. TYPE should be CONFIGURATION/EVENT/CONSUMPTION. If START and END are given, "
            + "only data from tacts number START to END are processed. START and END should be integer between 1 to current tact number\n"
            + "\"show resources\"\n\tShows current amount of resources in factory";

    private final Factory factory;
    private final Queue<String> commands;
    private boolean autopostpone;

    public TestFactoryUI(Factory factory) {
        this.factory = factory;
        commands = new LinkedList<>();
        this.factory.setMainUI(this);
    }

    public void addCommand(String input){
        this.commands.add(input);
    }

    @Override
    public void run() {
        String input;
        while (!commands.isEmpty()) {
            autopostpone = false;
            input = commands.poll().toLowerCase();
            if (isNextTactCommand(input)) {
                handleNextTactCommand(input);
            }
            else if (isHelpCommand(input)) {
                handleHelpCommand();
            }
            else if (isExitCommand(input)) {
                if (isExitConfirmed()) {
                    handleExitCommand();
                    return;
                }
            }
            else if (isContractCommand(input)) {
                handleContractCommand(input);
            }
            else if (isAddMoneyCommand(input)) {
                handleAddMoneyCommand(input);
            }
            else if (isReportCommand(input)) {
                handleReportCommand(input);
            }
            else if (isShowResourcesCommand(input)) {
                handleShowResourcesCommand();
            }
            else {
                handleInvalidCommand(input);
            }
        }
    }

    @Override
    public void getMessage(String msg) {
        System.out.println(msg);
    }

    @Override
    public String handleRequest(String request) {
        System.out.println(request);
        return commands.poll();
    }



    private boolean isNextTactCommand(String command) {
        String[] parts = command.split(" ");
        if (parts.length > 3 || parts.length < 1) {
            return false;
        }
        return parts[0].equals(NEXT_COMMAND_SHORT) || parts[0].equals(NEXT_COMMAND_LONG);
    }

    private boolean isHelpCommand(String command) {
        return HELP_COMMAND.equals(command);
    }

    private boolean isExitCommand(String command) {
        return EXIT_COMMAND.equals(command);
    }

    private boolean isContractCommand(String command) {
        return (command.startsWith(CONTRACT_COMMAND) && command.split(" ").length == 6);
    }

    private boolean isAddMoneyCommand(String command) {
        return (command.startsWith(ADD_MONEY_COMMAND) && command.split(" ").length == 3);
    }

    private boolean isYesCommand(String command) {
        return YES_COMMAND.equals(command);
    }

    private boolean isReportCommand(String command) {
        String[] parts = command.split(" ");
        return ((parts.length == 2 || parts.length == 4) && parts[0].equals(REPORT_COMMAND));
    }

    private boolean isShowResourcesCommand(String command) {
        return command.equals(SHOW_RESOURCES_COMMAND);
    }

    private boolean isExitConfirmed() {
        System.out.print("Type \"yes\" to confirm exiting the factory: ");
        String input = commands.poll().toLowerCase();
        return isYesCommand(input);
    }

    private void handleHelpCommand() {
        System.out.println(HELP_TEXT);
    }

    private void handleNextTactCommand(String command) {
        String[] parts = command.split(" ");
        // 1 arg - only one tact
        if (parts.length == 1) {
            factory.nextTact(autopostpone);
        }
        // 2 or 3 args - more tacts
        else if (parts.length == 2 || parts.length == 3) {
            int num;
            try {
                num = Integer.parseInt(parts[1]);
            }
            catch (NumberFormatException ex) {
                handleInvalidCommand(command);
                return;
            }
            if (parts.length == 3 && parts[2].equals("postpone")) {
                autopostpone = true;
            }
            if (num < 1) {
                handleInvalidCommand(command);
                return;
            }
            // does given amount of tacts
            for (int i = 0; i < num; ++i) {
                factory.nextTact(autopostpone);
            }
        }
        // 4 or more args - invalid command
        else {
            handleInvalidCommand(command);
        }
    }

    private void handleContractCommand(String command) {
        String[] parts = command.split(" ");
        Priority priority = null;
        for (Priority p : Priority.values()) {
            if (p.name().toLowerCase().equals(parts[2])) {
                priority = p;
                break;
            }
        }
        if (priority == null) {
            handleInvalidCommand(command);
            return;
        }
        ProductType type = null;
        for (ProductType pt : ProductType.values()) {
            if (pt.name().toLowerCase().equals(parts[3])) {
                type = pt;
                break;
            }
        }
        if (type == null) {
            handleInvalidCommand(command);
            return;
        }
        int price;
        int amount;
        try {
            price = Integer.parseInt(parts[4]);
            amount = Integer.parseInt(parts[5]);
            factory.contractCommand(priority, type, price, amount);
        }
        catch (IllegalArgumentException e) {
            handleInvalidCommand(command);
        }
    }

    private void handleAddMoneyCommand(String command) {
        String[] parts = command.split(" ");
        int amount;
        try {
            amount = Integer.parseInt(parts[2]);
            factory.addMoneyCommand(amount);
        }
        catch (IllegalArgumentException ex) {
            handleInvalidCommand(command);
        }
    }

    private void handleReportCommand(String command) {
        String[] parts = command.split(" ");
        ArchiveType report = null;
        for (ArchiveType a : ArchiveType.values()) {
            if (a.name().toLowerCase().equals(parts[1])) {
                report = a;
                break;
            }
        }
        if (report == null) {
            handleInvalidCommand(command);
        }
        if (parts.length == 4) {
            try {
                int start = Integer.parseInt(parts[2]);
                int end = Integer.parseInt(parts[3]);
                factory.reportCommand(report, start, end);
            }
            catch (IllegalArgumentException ex) {
                handleInvalidCommand(command);
            }
        }
        else {
            factory.reportCommand(report);
        }
    }

    private void handleShowResourcesCommand() {
        factory.showResourcesCommand();
    }

    private void handleExitCommand() {
        System.out.println("Factory exited after " + Factory.getCurrentTact() + " tacts.");
    }

    private void handleInvalidCommand(String s) {
        System.out.println("Invalid command, please check it and type again.");
    }
}
