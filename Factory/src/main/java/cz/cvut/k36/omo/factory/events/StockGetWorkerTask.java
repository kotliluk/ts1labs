
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.supply.Stock;

/**
 * Task for a Stock to get a Worker. Received Worker can be specified by an ID, or
 * it can be asked for any free Worker or it can be asked for all Workers in
 * the Stock.
 */
public class StockGetWorkerTask extends Task {

    public enum WorkerIdentification {
        ID,
        FREE,
        ALL;
    }

    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final WorkerIdentification workerIdentification;
    private final int workerID;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    /**
     * Creates a Task to get a FREE or ALL Workers, depending on the given
     * WorkerIdentification parameter.
     * 
     * @param sender task sender
     * @param priority task priority
     * @param identification FREE or ALL WorkerIdentification
     */
    public StockGetWorkerTask(EventChannelAccessible sender, Priority priority, WorkerIdentification identification) {
        super(priority, sender, Stock.class);
        this.workerIdentification = identification;
        this.workerID = -1;
    }
    
    /**
     * Creates a Task to get a Worker with the given ID from the Stock.
     * 
     * @param sender task sender
     * @param priority task priority
     * @param ID Wanted worker's ID
     */
    public StockGetWorkerTask(EventChannelAccessible sender, Priority priority, int ID) {
        super(priority, sender, Stock.class);
        this.workerIdentification = WorkerIdentification.ID;
        this.workerID = ID;
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */

    public WorkerIdentification getWorkerIdentification() {
        return workerIdentification;
    }

    /**
     * @return ID of the wanted Worker od -1 if getWorkerIdentification() != ID
     */
    public int getWorkerID() {
        return workerID;
    }
    
    @Override
    public String toString() {
        switch (workerIdentification) {
            case ID:
                return this.getClass().getSimpleName() + " from " + sender + " to get Worker with ID " + workerID;
            case FREE:
                return this.getClass().getSimpleName() + " from " + sender + " to get a free Worker";
            case ALL:
                return this.getClass().getSimpleName() + " from " + sender + " to get all Workers";
        }
        return this.getClass().getSimpleName();
    }
}

