/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.k36.omo.factory.archive;

import cz.cvut.k36.omo.factory.factory.Factory;
import cz.cvut.k36.omo.factory.archive.EventArchive.ReportEvent;
import cz.cvut.k36.omo.factory.archive.FactoryConfigurationArchive.HistoryConveyor;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author grigoal1
 */
public class Archive {
    
    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static final Logger LOG = Logger.getLogger(Archive.class.getName());
    private static Archive INSTANCE;

    /* ----------------------- STATIC METHODS ----------------------- */
    
    public synchronized static Archive getInstance(){
        if (INSTANCE == null) {
            INSTANCE = new Archive();
        }
        return INSTANCE;
    }

    public static void resetArchivesForTesting() {
        if (INSTANCE != null) {
            INSTANCE.archives.clear();
            INSTANCE.archives.add(new ConsumptionArchive());
            INSTANCE.archives.add(new EventArchive());
            INSTANCE.archives.add(new FactoryConfigurationArchive());
        }
    }
    
    /* ----------------------- INSTANCE'S FIELDS ----------------------- */
    
    private final List<SpecialArchive> archives = new ArrayList<>();

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    private Archive(){
        archives.add(new ConsumptionArchive());
        archives.add(new EventArchive());
        archives.add(new FactoryConfigurationArchive());
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */

    public SpecialArchive getSpecialArchive(ArchiveType at){
            return archives.stream()
                    .filter(p -> p.getType() == at)
                    .findFirst()
                    .orElse(null);
    }

    public String factoryConfigurationReport() {
        return processFactoryConfigurationReport(-1, -1);
    }

    public String factoryConfigurationReport(int start, int end){
        return processFactoryConfigurationReport(start, end);
    }

    public String eventReport(){
        return processEventReport(-1, -1);
    }

    public String eventReport(int start, int end){
        return processEventReport(start, end);
    }

    public String consumptionReport(){
        ConsumptionArchive consumptionReport = (ConsumptionArchive)getSpecialArchive(ArchiveType.CONSUMPTION);
        StringBuilder sb = new StringBuilder();
        sb.append("Consumption of resources in all tacts.");
        sb.append("\nTotal use of electricity: ").append(consumptionReport.totalUseOfElectricity());
        sb.append("\nTotal use of oil: ").append(consumptionReport.totalUseOfOil());
        sb.append("\nIndividual usages of resources:\n");
        consumptionReport.useOfMaterial().forEach(p -> sb.append(p).append("\n"));
        return saveReport(sb.toString(), "consumption_report_tacts-all_generated-" + Factory.getCurrentTact() + ".txt", "Consumption report");
    }
    
    public String consumptionReport(int start, int end){
        ConsumptionArchive consumptionReport = (ConsumptionArchive)getSpecialArchive(ArchiveType.CONSUMPTION);
        StringBuilder sb = new StringBuilder();
        sb.append("Consumption of resources in tacts ").append(start).append(" – ").append(end).append(".");
        sb.append("\nTotal use of electricity: ").append(consumptionReport.totalUseOfElectricity(start, end));
        sb.append("\nTotal use of oil: ").append(consumptionReport.totalUseOfOil(start, end));
        sb.append("\nIndividual usages of resources:\n");
        consumptionReport.useOfMaterial(start, end).forEach(p -> sb.append(p).append("\n"));
        return saveReport(sb.toString(), "consumption_report_tacts-" + start + "-" + end + "_generated-" + Factory.getCurrentTact() + ".txt", "Consumption report");
    }
    
    /* ----------------------- PRIVATE METHODS ----------------------- */
    
    private String processFactoryConfigurationReport(int start, int end) {
        List<HistoryConveyor> list;
        String filename;
        StringBuilder sb = new StringBuilder();
        if (start == -1) {
            sb.append("Configuration report for all tacts. All conveyors are counted.\n");
            filename = "config_report_tacts-all_generated-" + Factory.getCurrentTact() + ".txt";
            list = ((FactoryConfigurationArchive)getSpecialArchive(ArchiveType.CONFIGURATION)).getConfig();
        }
        else {
            sb.append("Configuration report for tacts ").append(start).append(" – ").append(end).append(". Only conveyors which existed in given period of tacts are counted.\n");
            filename = "config_report_tacts-" + start + "-" + end + "_generated-" + Factory.getCurrentTact() + ".txt";
            list = ((FactoryConfigurationArchive)getSpecialArchive(ArchiveType.CONFIGURATION)).getConfig(start, end);
        }
        sb.append("- Conveyors, which are still available:\n");
        StringBuilder sb1 = new StringBuilder("");
        list.stream()
                .filter(p -> p.conveyor != null)
                .forEach(p -> {
                    sb1.append(p.conveyorStr).append(" (created in tact ").append(p.start).append(") contains:\n");
                    sb1.append(p.workplaceStr);
                });
        if (sb1.toString().equals("")) {
            sb1.append("No such conveyor exists.\n");
        }
        sb.append(sb1);
        sb.append("- Conveyors, which has been decomposed:\n");
        StringBuilder sb2 = new StringBuilder("");
        list.stream()
                .filter(p -> p.conveyor == null)
                .forEach(p -> {
                    sb2.append(p.conveyorStr).append(" (existed in tacts ").append(p.start).append(" - ").append(p.end).append(") contained:\n");
                    sb2.append(p.workplaceStr);
                });
        if (sb2.toString().equals("")) {
            sb2.append("No such conveyor exists.\n");
        }
        sb.append(sb2);
        return saveReport(sb.toString(), filename, "Configuration report");
    }
    
    private String processEventReport(int start, int end) {
        List<ReportEvent> list;
        String filename;
        StringBuilder sb = new StringBuilder();
        if (start == -1) {
            sb.append("Event report for all tacts.\n");
            filename = "event_report_tacts-all_generated-" + Factory.getCurrentTact() + ".txt";
            list = ((EventArchive)getSpecialArchive(ArchiveType.EVENT)).getEvents();
        }
        else {
            sb.append("Event report for tacts ").append(start).append(" – ").append(end).append(".\n");
            filename = "event_report_tacts-" + start + "-" + end + "_generated-" + Factory.getCurrentTact() + ".txt";
            list = ((EventArchive)getSpecialArchive(ArchiveType.EVENT)).getEvents(start, end);
        }
        list.stream().forEach(p -> sb.append(p).append("\n"));
        return saveReport(sb.toString(), filename, "Event report");
    }
    
    /**
     * 
     * @param report
     * @param filename With .txt ending
     * @param reportType 
     */
    private String saveReport(String report, String filename, String reportType) {
        String ret;
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
            writer.write(report);
            writer.close();
            ret = reportType + " was saved into file: " + filename;
        } catch (IOException e) {
            LOG.log(Level.WARNING, "File {0}.txt cannot be created.", new Object[]{filename});
            ret = "Error while saving a report, please try again";
        }
        return ret;
    }
}
