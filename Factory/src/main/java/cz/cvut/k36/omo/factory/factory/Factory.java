
package cz.cvut.k36.omo.factory.factory;

import cz.cvut.k36.omo.factory.archive.Archive;
import cz.cvut.k36.omo.factory.archive.ArchiveType;
import cz.cvut.k36.omo.factory.contract.ConflictSolution;
import cz.cvut.k36.omo.factory.contract.Contract;
import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.events.ContractConflictResponse;
import cz.cvut.k36.omo.factory.events.ContractConflictTask;
import cz.cvut.k36.omo.factory.events.EventChannel;
import cz.cvut.k36.omo.factory.events.EventChannelAccessible;
import cz.cvut.k36.omo.factory.events.MessageToUserTask;
import cz.cvut.k36.omo.factory.events.MoneyNeededResponse;
import cz.cvut.k36.omo.factory.events.MoneyNeededTask;
import cz.cvut.k36.omo.factory.events.NewContractResponse;
import cz.cvut.k36.omo.factory.events.NewContractTask;
import cz.cvut.k36.omo.factory.events.StockResourceTask;
import cz.cvut.k36.omo.factory.events.Response;
import cz.cvut.k36.omo.factory.events.StockResourceResponse;
import cz.cvut.k36.omo.factory.events.Task;
import cz.cvut.k36.omo.factory.product.ProductType;
import cz.cvut.k36.omo.factory.production.Production;
import cz.cvut.k36.omo.factory.supply.ResourceType;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Factory itself. It can be controlled by FactoryUIWrapper only.
 */
public class Factory implements EventChannelAccessible {

    /* ----------------------- STATIC FIELDS ----------------------- */

    private static int currentTact = 0;
    private static final Logger LOG = Logger.getLogger(Factory.class.getName());

    /* ----------------------- STATIC METHODS ----------------------- */

    public static int getCurrentTact() {
        return currentTact;
    }

    /* ----------------------- INSTANCES' FIELDS ----------------------- */

    private final EventChannel eventChannel;
    private Response response;

    private FactoryUIWrapper mainUI;

    private final Production production;
    
    private boolean autopostpone;

    /* ----------------------- TESTING FIELDS ----------------------- */

    private Integer fixedConflictSolution;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */

    public Factory(Production production, EventChannel eventChannel) {
        currentTact = 0;
        this.eventChannel = eventChannel;
        this.eventChannel.attach(this);
        this.production = production;
    }

    /* ----------------------- TESTING PUBLIC METHODS ----------------------- */

    public void setFixedConflictSolution(Integer number) {
        fixedConflictSolution = number;
    }

    public void clearFixedConflictSolution() {
        fixedConflictSolution = null;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */

    @Override
    public void processTask(Task task) {
        if (task instanceof ContractConflictTask) {
            processContractConflictTask((ContractConflictTask)task);
        }
        else if (task instanceof MoneyNeededTask) {
            processMoneyNeededTask((MoneyNeededTask)task);
        }
        else if (task instanceof MessageToUserTask) {
            processMessageToUserTask((MessageToUserTask)task);
        }
        else {
            LOG.log(Level.WARNING, "{0} received an unexpected task: {1}", new Object[]{this, task});
        }
    }

    @Override
    public void receiveResponse(Response response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "Factory";
    }

    /* ----------------------- PACKAGE METHODS ----------------------- */

    /**
     * Sets main Factory's UI, which will receive all messages and requests from Factory.
     * @param ui
     */
    void setMainUI(FactoryUIWrapper ui) {
        mainUI = ui;
    }

    void nextTact(boolean postpone) {
        autopostpone = postpone;
        ++currentTact;
        LOG.log(Level.INFO, "Tact {0} started", currentTact);

        StringBuilder sb = new StringBuilder();
        mainUI.getMessage("********** Tact " + currentTact + ": **********");
        production.nextTact();
        if (currentTact % 160 == 0) {
            LOG.log(Level.INFO, "Tact {0} is paying off", currentTact);
            production.accountingTact();
        }
        LOG.log(Level.INFO, "Tact {0} ended", currentTact);
    }

    void showResourcesCommand() {
        int money = 0;
        int electricity = 0;
        int oil = 0;
        int steal = 0;
        eventChannel.addTask(new StockResourceTask(this, Priority.NORMAL, ResourceType.MONEY));
        if (response instanceof StockResourceResponse) {
            money = ((StockResourceResponse)response).getResponseAmount();
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StockResourceResponse", this);
        }
        response = null;

        eventChannel.addTask(new StockResourceTask(this, Priority.NORMAL, ResourceType.ELECTRICITY));
        if (response instanceof StockResourceResponse) {
            electricity = ((StockResourceResponse)response).getResponseAmount();
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StockResourceResponse", this);
        }
        response = null;

        eventChannel.addTask(new StockResourceTask(this, Priority.NORMAL, ResourceType.OIL));
        if (response instanceof StockResourceResponse) {
            oil = ((StockResourceResponse)response).getResponseAmount();
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StockResourceResponse", this);
        }
        response = null;

        eventChannel.addTask(new StockResourceTask(this, Priority.NORMAL, ResourceType.STEAL));
        if (response instanceof StockResourceResponse) {
            steal = ((StockResourceResponse)response).getResponseAmount();
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StockResourceResponse", this);
        }
        response = null;

        mainUI.getMessage("Factory has " + money + " money, " + electricity + " electricity, " + oil + " oil, " + steal + " steal.");
    }

    void contractCommand(Priority priority, ProductType type, int price, int amount) throws IllegalArgumentException {
        if (price < 1) {
            mainUI.getMessage("Given contract's price must be positive.");
            throw new IllegalArgumentException();
        }
        if (amount < 1) {
            mainUI.getMessage("Given contract's amount must be positive.");
            throw new IllegalArgumentException();
        }
        if (priority == null || priority == Priority.NO) {
            mainUI.getMessage("Given contract's priority must be set.");
            throw new IllegalArgumentException();
        }
        if (type == null) {
            mainUI.getMessage("Given contract's type must be set.");
            throw new IllegalArgumentException();
        }
        Contract contract = new Contract(priority, type, price, amount);
        eventChannel.addTask(new NewContractTask(this, contract));
        if (response instanceof NewContractResponse) {
            if (((NewContractResponse)response).wasContractAdded()) {
                mainUI.getMessage("Given contract was added into the Production as pending. Production will try to start it in next tact.");
            }
            else {
                mainUI.getMessage("Given contract was not added into the Production. It was unfavorable.");
            }
            response = null;
        }
        else {
            LOG.warning("Factory did not received a response after NewContractTask");
            mainUI.getMessage("Error while adding a Contract, try again.");
        }
    }

    void addMoneyCommand(int amount) throws IllegalArgumentException {
        if (amount < 0) {
            mainUI.getMessage("Given amount of money to be added cannot be negative");
            throw new IllegalArgumentException();
        }
        eventChannel.addTask(new StockResourceTask(this, Priority.NORMAL, ResourceType.MONEY, StockResourceTask.Action.ADD, amount));
        mainUI.getMessage(amount + " money added");
    }

    void reportCommand(ArchiveType type) {
        switch (type) {
            case CONFIGURATION:
                mainUI.getMessage(Archive.getInstance().factoryConfigurationReport());
                break;
            case EVENT:
                mainUI.getMessage(Archive.getInstance().eventReport());
                break;
            case CONSUMPTION:
                mainUI.getMessage(Archive.getInstance().consumptionReport());
                break;
            default:
                mainUI.getMessage("Error while showing a report for \"" + type + "\", give a valid report type.");
        }
    }

    void reportCommand(ArchiveType type, int start, int end) throws IllegalArgumentException {
        if (start < 1 || start > currentTact || end < 1 || end > currentTact) {
            mainUI.getMessage("Given starting and ending tacts must have already happened: tacts " + 1 + " to " + currentTact + ".");
            throw new IllegalArgumentException();
        }
        if (start > end) {
            mainUI.getMessage("Starting tact of reporting period cannot be higher than ending tact.");
            throw new IllegalArgumentException();
        }
        switch (type) {
            case CONFIGURATION:
                mainUI.getMessage(Archive.getInstance().factoryConfigurationReport(start, end));
                break;
            case EVENT:
                mainUI.getMessage(Archive.getInstance().eventReport(start, end));
                break;
            case CONSUMPTION:
                mainUI.getMessage(Archive.getInstance().consumptionReport(start, end));
                break;
            default:
                mainUI.getMessage("Error while showing a report for \"" + type + "\", give a valid report type.");
        }
    }

    /* ----------------------- PRIVATE METHODS ----------------------- */

    private void processContractConflictTask(ContractConflictTask cct) {
        if (autopostpone) {
            mainUI.getMessage("Contract request for \"" + cct.getContract() + "\" was automatically postponed.");
            eventChannel.addResponse(new ContractConflictResponse(this, cct, new ConflictSolution()));
            return;
        }
        List<ConflictSolution> solutions = cct.getSolutions();
        StringBuilder msg = new StringBuilder();
        msg.append("There is a Contract request for \"" + cct.getContract() + "\", which cannot be satisfied automatically. Please choose a solution from:").append(System.lineSeparator());
        int num = 0;
        for (ConflictSolution cs : solutions) {
            ++num;
            msg.append("\t" + num + ") " + cs.toString()).append(System.lineSeparator());
        }
        mainUI.getMessage(msg.toString());

        // ADDED FOR TESTING
        if (fixedConflictSolution != null) {
            if (fixedConflictSolution > 0 && fixedConflictSolution <= num) {
                mainUI.getMessage("Type number of your chosen solution (1 - " + num + "): ");
                eventChannel.addResponse(new ContractConflictResponse(this, cct, solutions.get(fixedConflictSolution - 1)));
                return;
            }
            else {
                throw new IllegalArgumentException("Set fixedConflictSolution is not in current conflict range");
            }
        }

        int choice = 0;
        while (true) {
            String input = mainUI.handleRequest("Type number of your chosen solution (1 - " + num + "): ");

            try {
                choice = Integer.parseInt(input);
            }
            catch (NumberFormatException ex) { }
            if (choice > 0 && choice <= num) {
                eventChannel.addResponse(new ContractConflictResponse(this, cct, solutions.get(choice - 1)));
                break;
            }
        }
    }

    private void processMoneyNeededTask(MoneyNeededTask mnt) {
        mainUI.getMessage("The factory needs " + mnt.getAmount() + " of money to work on.");
        int choice = -1;
        while (true) {
            String input = mainUI.handleRequest("Give at least the needed amount of money to the factory (type " + mnt.getAmount() + " or higner number)\nor let the factory bankrupt and destroy (type \"bankrupt\"): ");
            if (input.equals("bankrupt")) {
                mainUI.getMessage("\nThe factory did not make enough money to survive in our cruel world...\nIt has survived for " + currentTact + " tacts. R.I.P. our factory :(");
                System.exit(0);
            }
            try {
                choice = Integer.parseInt(input);
            }
            catch (NumberFormatException ex) { }
            if (choice >= mnt.getAmount()) {
                eventChannel.addResponse(new MoneyNeededResponse(this, mnt, choice));
                break;
            }
        }
    }

    private void processMessageToUserTask(MessageToUserTask mtut) {
        mainUI.getMessage(mtut.getMessage());
    }
}
