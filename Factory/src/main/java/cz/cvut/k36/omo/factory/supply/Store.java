
package cz.cvut.k36.omo.factory.supply;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.events.StoreWorkplaceResponse;
import cz.cvut.k36.omo.factory.events.StoreWorkplaceTask;
import cz.cvut.k36.omo.factory.events.EventChannelAccessible;
import cz.cvut.k36.omo.factory.events.EventChannel;
import cz.cvut.k36.omo.factory.events.Response;
import cz.cvut.k36.omo.factory.events.StockResourceResponse;
import cz.cvut.k36.omo.factory.events.StockResourceTask;
import cz.cvut.k36.omo.factory.events.StoreResourceResponse;
import cz.cvut.k36.omo.factory.events.StoreResourceTask;
import cz.cvut.k36.omo.factory.events.Task;
import cz.cvut.k36.omo.factory.production.Machine;
import cz.cvut.k36.omo.factory.production.MachineType;
import cz.cvut.k36.omo.factory.production.Robot;
import cz.cvut.k36.omo.factory.production.RobotType;
import cz.cvut.k36.omo.factory.production.Workplace;
import cz.cvut.k36.omo.factory.production.WorkplaceType;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Sells Workplaces and resources needed in the Factory.
 */
public class Store implements EventChannelAccessible {

    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static final Logger LOG = Logger.getLogger(Store.class.getName());
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final EventChannel eventChannel;
    private Response response;
    
    private final Map<WorkplaceType, Integer> workplacesPrices;
    private final Map<ResourceType, Integer> resourcesPrices;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public Store(EventChannel eventChannel, Map<WorkplaceType, Integer> workplacesPrices, Map<ResourceType, Integer> resourcesPrices) {
        this.eventChannel = eventChannel;
        this.eventChannel.attach(this);
        this.workplacesPrices = workplacesPrices;
        this.resourcesPrices = resourcesPrices;
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */

    @Override
    public void processTask(Task task) {
        if (task instanceof StoreWorkplaceTask) {
            processBuyWorkplaceTask((StoreWorkplaceTask)task);
        }
        else if (task instanceof StoreResourceTask) {
            processStoreResourceTask((StoreResourceTask)task);
        }
        else {
            LOG.log(Level.WARNING, "{0} received an unexpected task: {1}", new Object[]{this, task});
        }
    }

    @Override
    public void receiveResponse(Response response) {
        this.response = response;
    }
    
    @Override
    public String toString() {
        return "Store";
    }

    /* ----------------------- PRIVATE METHODS ----------------------- */
    
    /**
     * @param rt ResourceType which is asked for
     * @return Cost of the asked ResourceType
     * @throws NoSuchElementException If the asked ResourceType is not sold
     */
    private int getResourcePrice(ResourceType rt) throws NoSuchElementException {
        Integer price = resourcesPrices.get(rt);
        if (price == null) {
            throw new NoSuchElementException();
        }
        return price;
    }
    
    /**
     * Sells the given amount of the given type of Resource if there is enough
     * money in the Stock and if the Store sells the given type of Resource.
     * Otherwise, 0 is returned. To be sure to get
     * wanted amount of wanted Resource, before calling this function check that
     * there is more or equal money in the stock than return of the call
     * Store.getResourcePrice(rt) multiplied by amount and that the call
     * Store.getResourcePrice(rt) does not throw an NoSuchElementException
     * exception.
     * 
     * @param rt Type of the wanted Resource
     * @param amount Amount of the wanted Resource
     * @return amount if the transaction was successfull or 0, if
     * there is not enough money in the Stock or the Store does not sell
     * given type of the Resource
     */
    private int sellResource(ResourceType rt, int amount) {
        int cost;
        try {
            cost = getResourcePrice(rt) * amount;
        }
        catch (NoSuchElementException ex) {
            return 0;
        }
        
        int money = 0;
        eventChannel.addTask(new StockResourceTask(this, Priority.NORMAL, ResourceType.MONEY, StockResourceTask.Action.USE, cost));
        if (response instanceof StockResourceResponse) {
            money = ((StockResourceResponse)response).getResponseAmount();
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StockResourceResponse", this);
        }
        response = null;
        // there was not enough money in the Stock
        if (money < cost) {
            return 0;
        }
        
        return amount;
    }
    
    /**
     * @param wt WorkplaceType which is asked for
     * @return Cost of the asked WorkplaceType
     * @throws NoSuchElementException If the asked WorkplaceType is not sold
     */
    private int getWorkplacePrice(WorkplaceType wt) throws NoSuchElementException {
        Integer price = workplacesPrices.get(wt);
        if (price == null) {
            throw new NoSuchElementException();
        }
        return price;
    }
    
    /**
     * Sells the given type of the Workplace if there is enough money in the
     * Stock and if the Store sells the given type of the Workplace.
     * Otherwise, null is returned. To be sure to get
     * a valid instance of Workplace, before calling this function check that
     * there is more or equal money in the stock than call of
     * Store.getWorkplacePrice(wt) returns and that the call
     * Store.getWorkplacePrice(wt) does not throw an NoSuchElementException
     * exception.
     * 
     * @param wt Type of the wanted Workplace
     * @return New Workplace instance of the given WorkplaceType or null, if
     * there is not enough money in the Stock or the Store does not sell
     * given type of the Workplace
     */
    private Workplace sellWorkplace(WorkplaceType wt) {
        int cost;
        try {
            cost = getWorkplacePrice(wt);
        }
        catch (NoSuchElementException ex) {
            return null;
        }
        
        int money = 0;
        eventChannel.addTask(new StockResourceTask(this, Priority.NORMAL, ResourceType.MONEY, StockResourceTask.Action.USE, cost));
        if (response instanceof StockResourceResponse) {
            money = ((StockResourceResponse)response).getResponseAmount();
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StockResourceResponse", this);
        }
        response = null;
        // there was not enough money in the Stock
        if (money < cost) {
            return null;
        }
        
        LOG.log(Level.FINE, "Store sells {0} for {1}", new Object[]{wt, cost});
        if (wt.isMachineType()) {
            return new Machine((MachineType)wt);
        }
        else {
            return new Robot((RobotType)wt);
        }
    }
    
    private void processBuyWorkplaceTask(StoreWorkplaceTask swt) {
        switch (swt.getAction()) {
            case BUY:
                LOG.log(Level.FINEST, "Store received a buying request for {0}", swt.getWorkplaceType());
                eventChannel.addResponse(new StoreWorkplaceResponse(this, swt, sellWorkplace(swt.getWorkplaceType()), getWorkplacePrice(swt.getWorkplaceType())));
                break;
            case GET_PRICE:
                LOG.log(Level.FINEST, "Store received a price getting request for {0}", swt.getWorkplaceType());
                eventChannel.addResponse(new StoreWorkplaceResponse(this, swt, null, getWorkplacePrice(swt.getWorkplaceType())));
                break;
        }
    }

    private void processStoreResourceTask(StoreResourceTask srt) {
        switch (srt.getAction()) {
            case BUY:
                LOG.log(Level.FINEST, "Store received a buying request for {0}", srt.getResourceType());
                eventChannel.addResponse(new StoreResourceResponse(this, srt, sellResource(srt.getResourceType(), srt.getAmount()), getResourcePrice(srt.getResourceType())));
                break;
            case GET_PRICE:
                LOG.log(Level.FINEST, "Store received a price getting request for {0}", srt.getResourceType());
                eventChannel.addResponse(new StoreResourceResponse(this, srt, 0, getResourcePrice(srt.getResourceType())));
                break;
        }
    }
}
