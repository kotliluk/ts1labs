
package cz.cvut.k36.omo.factory.supply;

/**
 * Types of resources used in the factory: MONEY, ELECTRICITY, OIL, STEAL.
 */
public enum ResourceType {
    MONEY(1),
    ELECTRICITY(1),
    OIL(2),
    STEAL(3);
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final int price;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    private ResourceType(int price) {
        this.price = price;
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    /**
     * @return Base price of the resource (used to compute a real used price in
     * Store)
     */
    public int getBasePrice() {
        return price;
    }
}
