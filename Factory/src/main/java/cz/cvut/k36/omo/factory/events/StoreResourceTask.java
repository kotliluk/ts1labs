
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.supply.ResourceType;
import cz.cvut.k36.omo.factory.supply.Store;

/**
 * Task for a Store to buy resource or to get its price.
 */
public class StoreResourceTask extends Task {
    
    public enum Action {
        BUY,
        GET_PRICE;
    }
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final ResourceType resourceType;
    private final Action action;
    private final int amount;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    /**
     * Creates a task to buy given amount of the given resource from the Store.
     * 
     * @param sender task's sender
     * @param priority task's priority
     * @param resourceType type of the wanted resource
     * @param amount amount of wanted the resource
     */
    public StoreResourceTask(EventChannelAccessible sender, Priority priority, ResourceType resourceType, int amount) {
        super(priority, sender, Store.class);
        this.resourceType = resourceType;
        this.amount = amount;
        this.action = Action.BUY;
    }

    /**
     * Creates a task to get price of the given resource in the Store.
     * 
     * @param sender task's sender
     * @param priority task's priority
     * @param resourceType type of the wanted resource
     */
    public StoreResourceTask(EventChannelAccessible sender, Priority priority, ResourceType resourceType) {
        super(priority, sender, Store.class);
        this.resourceType = resourceType;
        this.amount = 0;
        this.action = Action.GET_PRICE;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public ResourceType getResourceType() {
        return resourceType;
    }

    public int getAmount() {
        return amount;
    }

    public Action getAction() {
        return action;
    }
    
    @Override
    public String toString() {
        switch (action) {
            case BUY:
                return this.getClass().getSimpleName() + " from " + sender + " to buy " + amount + " of " + resourceType;
            case GET_PRICE:
                return this.getClass().getSimpleName() + " from " + sender + " to get price of " + resourceType;
        }
        return this.getClass().getSimpleName();
    }
}