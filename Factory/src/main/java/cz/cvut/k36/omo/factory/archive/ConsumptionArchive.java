package cz.cvut.k36.omo.factory.archive;

import cz.cvut.k36.omo.factory.factory.Factory;
import cz.cvut.k36.omo.factory.supply.ResourceType;
import cz.cvut.k36.omo.factory.production.Workplace;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ConsumptionArchive implements SpecialArchive {
    
    class ResourceConsumption {
        String wp;
        ResourceType rt;
        int material;
        int tact;
        public ResourceConsumption(Workplace wp, ResourceType rt, int material, int tact) {
            this.wp = wp.toString();
            this.rt = rt;
            this.material = material;
            this.tact = tact;
        }
        @Override
        public String toString() {
            return "Tact " + tact + ": " + wp + " consumed " + material + " of " + rt;
        }
    }
    
    /* ----------------------- INSTANCE'S FIELDS ----------------------- */
    
    final List<ResourceConsumption> history;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    ConsumptionArchive(){
        history = new ArrayList<>();
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public void addLogInArchive(Workplace wp, ResourceType rt , int material){
        history.add(new ResourceConsumption(wp, rt, material, Factory.getCurrentTact()));
    }

    @Override
    public ArchiveType getType() {
        return ArchiveType.CONSUMPTION;
    }

    /* ----------------------- PACKAGE METHODS ----------------------- */
    
    public int totalUseOfElectricity() {
        return history.stream()
                .filter(p -> p.rt == ResourceType.ELECTRICITY)
                .mapToInt(p -> p.material)
                .sum();
    }

    public int totalUseOfElectricity(int start, int end) {
        return editHistory(start, end).stream()
                .filter(p -> p.rt == ResourceType.ELECTRICITY)
                .mapToInt(p -> p.material)
                .sum();
    }

    public int totalUseOfOil() {
        return history.stream()
                .filter(p -> p.rt == ResourceType.OIL)
                .mapToInt(p -> p.material)
                .sum();
    }

    public int totalUseOfOil(int start, int end) {
        return editHistory(start, end).stream()
                .filter(p -> p.rt == ResourceType.OIL)
                .mapToInt(p -> p.material)
                .sum();
    }

    public List<ResourceConsumption> useOfMaterial() {
        List<ResourceConsumption> ret = new ArrayList<>(history.size());
        ret.addAll(history);
        return ret;
    }
    
    public List<ResourceConsumption> useOfMaterial(int start, int end) {
        List<ResourceConsumption> ret = new ArrayList<>();
        ret.addAll(editHistory(start, end));
        return ret;
    }

    /* ----------------------- PRIVATE METHODS ----------------------- */
    
    private List<ResourceConsumption> editHistory(int start, int end){
        return history.stream()
                .filter(p -> p.tact >= start && p.tact <= end)
                .collect(Collectors.toList());
    }
}
