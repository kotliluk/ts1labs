
package cz.cvut.k36.omo.factory.production;

import cz.cvut.k36.omo.factory.archive.Archive;
import cz.cvut.k36.omo.factory.archive.ArchiveType;
import cz.cvut.k36.omo.factory.archive.FactoryConfigurationArchive;
import cz.cvut.k36.omo.factory.contract.ConflictSolution;
import cz.cvut.k36.omo.factory.contract.Contract;
import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.events.StoreWorkplaceResponse;
import cz.cvut.k36.omo.factory.events.StoreWorkplaceTask;
import cz.cvut.k36.omo.factory.events.ContractConflictResponse;
import cz.cvut.k36.omo.factory.events.EventChannelAccessible;
import cz.cvut.k36.omo.factory.events.ContractConflictTask;
import cz.cvut.k36.omo.factory.events.EventChannel;
import cz.cvut.k36.omo.factory.events.MessageToUserTask;
import cz.cvut.k36.omo.factory.events.NewContractResponse;
import cz.cvut.k36.omo.factory.events.NewContractTask;
import cz.cvut.k36.omo.factory.events.Response;
import cz.cvut.k36.omo.factory.events.StockAddWorkplaceTask;
import cz.cvut.k36.omo.factory.events.StockGetWorkerResponse;
import cz.cvut.k36.omo.factory.events.StockGetWorkerTask;
import cz.cvut.k36.omo.factory.events.StockGetWorkplaceResponse;
import cz.cvut.k36.omo.factory.events.StockGetWorkplaceTask;
import cz.cvut.k36.omo.factory.events.StockResourceTask;
import cz.cvut.k36.omo.factory.events.StoreResourceResponse;
import cz.cvut.k36.omo.factory.events.StoreResourceTask;
import cz.cvut.k36.omo.factory.events.Task;
import cz.cvut.k36.omo.factory.product.ProductType;
import cz.cvut.k36.omo.factory.supply.ResourceType;
import cz.cvut.k36.omo.factory.worker.Accounting;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Class managing Conveyors and Contract producing. Minimal profit in contract to be accepted is 50%.
 */
public class Production implements EventChannelAccessible {

    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static final Logger LOG = Logger.getLogger(Production.class.getName());
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final List<Conveyor> conveyors;
    private final List<Contract> pendingContracts;
    private final Accounting accounting;
    private final int percentProfit;
    
    private final EventChannel eventChannel;
    private Response response = null;
    
    private final FactoryConfigurationArchive factoryConfigurationArchive;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public Production(EventChannel eventChannel, Accounting accounting) {
        this.eventChannel = eventChannel;
        this.eventChannel.attach(this);
        
        this.conveyors = new LinkedList<>();
        this.pendingContracts = new LinkedList<>();
        this.accounting = accounting;
        this.percentProfit = 50;
        factoryConfigurationArchive = (FactoryConfigurationArchive)(Archive.getInstance().getSpecialArchive(ArchiveType.CONFIGURATION));
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    /**
     * Processes next serie of Products in Conveyors and tries to begin pending
     * Contracts again.
     */
    public void nextTact() {       
        startPendingContracts();
        
        conveyors.stream().forEach((c) -> {
            c.process();
        });
    }
    
    /**
     * Pays off wages of all Employees in the factory.
     */
    public void accountingTact() {
        int payed = 0;

        eventChannel.addTask(new StockGetWorkerTask(this, Priority.NORMAL, StockGetWorkerTask.WorkerIdentification.ALL));
        if (response instanceof StockGetWorkerResponse) {
            payed += accounting.payWorkerWages(((StockGetWorkerResponse)response).getWorkers());
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StockGetWorkerResponse", this);
        }
        response = null;
        
        LOG.log(Level.INFO, "Production payed off {0} for employees", payed);
        eventChannel.addTask(new MessageToUserTask(this, Priority.NORMAL, "Accounting tact: " + payed + " payed to employees of the Factory"));
        if (payed > 0) {
            eventChannel.addTask(new StockResourceTask(this, Priority.NORMAL, ResourceType.MONEY, StockResourceTask.Action.USE, payed));
        }
    }
    
    /**
     * Exports finished Contract.
     * 
     * @param contract Finished contract
     */
    public void contractDone(Contract contract) {
        LOG.log(Level.INFO, "{0} finished", contract);
        eventChannel.addTask(new MessageToUserTask(this, Priority.NORMAL, contract + " finished"));
        // half of the Contract money is added before, half after
        eventChannel.addTask(new StockResourceTask(this, contract.getPriority(), ResourceType.MONEY, StockResourceTask.Action.ADD, contract.getProductSellingPrice() * contract.getOrderedCount() / 2));
    }

    @Override
    public void processTask(Task task) {
        if (task instanceof NewContractTask) {
            processNewContractTask((NewContractTask)task);
        }
        else {
            LOG.log(Level.WARNING, "{0} received an unexpected task: {1}", new Object[]{this, task});
        }
    }

    @Override
    public void receiveResponse(Response response) {
        this.response = response;
    }
    
    @Override
    public String toString() {
        return "Production";
    }

    /* ----------------------- PRIVATE METHODS ----------------------- */
    
    /**
     * Adds the given Contract as pending if it is favorable.
     * 
     * @param contract New received Contract
     * @return true if added, false otherwise
     */
    private boolean addContract(Contract contract){
        if (contract == null) {
            return false;
        }
        int minMoney = computeProductCosts(contract.getOrderedType()) * (100 + percentProfit) / 100;
        if (minMoney > contract.getProductSellingPrice()) {
            eventChannel.addTask(new MessageToUserTask(this, contract.getPriority(), "Minimal price for a " + contract.getOrderedType() + " is " + minMoney + "."));
            return false;
        }
        pendingContracts.add(contract);
        // half of the Contract money is added before, half after
        eventChannel.addTask(new StockResourceTask(this, contract.getPriority(), ResourceType.MONEY, StockResourceTask.Action.ADD, contract.getProductSellingPrice() * contract.getOrderedCount() / 2));
        return true;
    }

    private int computeProductCosts(ProductType pt) {
        int stealPrice = ResourceType.STEAL.getBasePrice();
        int electricityPrice = ResourceType.STEAL.getBasePrice();
        int oilPrice = ResourceType.STEAL.getBasePrice();
        
        eventChannel.addTask(new StoreResourceTask(this, Priority.NORMAL, ResourceType.STEAL));
        if (response instanceof StoreResourceResponse) {
            stealPrice = ((StoreResourceResponse)response).getPrice();
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StoreResourceResponse", this);
        }
        response = null;
        
        eventChannel.addTask(new StoreResourceTask(this, Priority.NORMAL, ResourceType.ELECTRICITY));
        if (response instanceof StoreResourceResponse) {
            electricityPrice = ((StoreResourceResponse)response).getPrice();
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StoreResourceResponse", this);
        }
        response = null;
        
        eventChannel.addTask(new StoreResourceTask(this, Priority.NORMAL, ResourceType.OIL));
        if (response instanceof StoreResourceResponse) {
            oilPrice = ((StoreResourceResponse)response).getPrice();
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StoreResourceResponse", this);
        }
        response = null;
        
        int oneProduct = pt.getStealNeeded() * stealPrice;
        for (WorkplaceType wt : pt.getConveyorWorkplaces()) {
            oneProduct += wt.getElectricityConsumption() * electricityPrice;
            oneProduct += wt.getOilConsumption() * oilPrice;
            if (wt.isMachineType()) {
                oneProduct += accounting.getWorkerWages();
            }
        }
        LOG.log(Level.INFO, "Costs for 1 {0} are: {1}", new Object[]{pt, oneProduct});
        return oneProduct;
    }

    private void startPendingContracts() {
        List<Conveyor> freeConveyors = conveyors.stream()
                .filter(c -> !c.hasContract())
                .collect(Collectors.toList());
        
        Priority[] priorities = Priority.values();
        // higher priorities first
        for (Priority priority : priorities) {
            // iterates through all pending contracts in priority order
            List<Contract> contracts = pendingContracts.stream()
                    .filter(c -> c.getPriority() == priority)
                    .collect(Collectors.toList());
            for (Contract c : contracts) {
                LOG.log(Level.FINE, "Production tries to start a new Contract {0}", c);
                // if there was no free conveyor for this type of the contract
                if (!tryInsertContractIntoFreeConveyor(c, freeConveyors)) {
                    // tries to get free workplaces from the Stock
                    WorkplaceType[] workplacesTypes = c.getOrderedType().getConveyorWorkplaces();
                    Map<WorkplaceType, Integer> needed = new HashMap<>();
                    List<Workplace> found = new ArrayList<>(workplacesTypes.length);
                    for (int i = 0; i < workplacesTypes.length; ++i) {
                        Integer count = needed.get(workplacesTypes[i]);
                        if (count == null) {
                            needed.put(workplacesTypes[i], 1);
                        }
                        else {
                            needed.put(workplacesTypes[i], count + 1);
                        }
                    }
                    for (WorkplaceType wt : needed.keySet()) {
                        Integer count = needed.get(wt);
                        eventChannel.addTask(new StockGetWorkplaceTask(this, c.getPriority(), wt, count));
                        if (response instanceof StockGetWorkplaceResponse) {
                            StockGetWorkplaceResponse swgr = (StockGetWorkplaceResponse) response;
                            needed.put(wt, count - swgr.getWorkplaces().size());
                            found.addAll(swgr.getWorkplaces());
                        }
                        else {
                            LOG.log(Level.WARNING, "{0} did not received a StockGetWorkplaceResponse", this);
                        }
                        response = null;
                    }
                    addFromEmptyConveyors(c, found, needed);
                    
                    // case of founding all needed workplaces in the stock
                    if (found.size() == workplacesTypes.length) {
                        createNewConveyor(found, c);
                    }
                    // user is asked to choose the solution of the situation
                    else {
                        LOG.fine("New Contract cannot be started automatically at the moment");
                        
                        eventChannel.addTask(userConflictSolution(c, needed));
                        if (response instanceof ContractConflictResponse) {
                            ContractConflictResponse ccr = (ContractConflictResponse)response;
                            processConflictSolution(ccr.getSolution(), c, found);
                        }
                        else {
                            LOG.log(Level.WARNING, "{0} did not received a ContractConflictResponse", this);
                        }
                        response = null;
                    }
                }
            }
        }
    }
    
    private boolean tryInsertContractIntoFreeConveyor(Contract contract, List<Conveyor> freeConveyors) {
        for (Conveyor conveyor : freeConveyors) {
            if (conveyor.getProductType() == contract.getOrderedType()) {
                conveyor.addContract(contract);
                freeConveyors.remove(conveyor);
                pendingContracts.remove(contract);
                LOG.fine("There is a free Conveyor for the Contract, Contract added");
                return true;
            }
        }
        LOG.finest("There is not a free Conveyor for the Contract");
        return false;
    }

    private void createNewConveyor(List<Workplace> workplaces, Contract contract) {
        Conveyor newConveyor = null;
        try {
            newConveyor = new Conveyor(this, contract, eventChannel, workplaces);
        }
        catch (IllegalArgumentException ex) {
            LOG.warning("Found Workplaces for a new Conveyor was not enough to build it");
            return;
        }
        pendingContracts.remove(contract);
        conveyors.add(newConveyor);
        factoryConfigurationArchive.addConveyor(newConveyor);
        LOG.fine("Conveyor for the Contract created from free Workplaces in the Stock");

    }

    /**
     * 
     * @param con
     * @param needed
     * @return Number of Workplaces from the Conveyor con, which are needed
     */
    private int hasNeededWorkplaces(Conveyor con, Map<WorkplaceType, Integer> needed) {
        WorkplaceType[] owned = con.getProductType().getConveyorWorkplaces();
        Boolean[] used = new Boolean[owned.length];
        int ret = 0;
        for (WorkplaceType wt : needed.keySet()) {
            for (int k = 0; k < needed.get(wt); ++k) {
                for (int i = 0; i < owned.length; ++i) {
                    if (owned[i] == wt && used[i] == null) {
                        used[i] = true;
                        ret += 1;
                        break;
                    }
                }
            }
        }
        return ret;
    }

    private ContractConflictTask userConflictSolution(Contract c, Map<WorkplaceType, Integer> needed) {
        List<ConflictSolution> solutions = new LinkedList<>();
        
        // postponing the contract
        solutions.add(new ConflictSolution());
        
        // buying new workplaces to create a new conveyor
        int cost = 0;
        for (WorkplaceType wt : needed.keySet()) {
            if (needed.get(wt) == 0) {
                continue;
            }
            eventChannel.addTask(new StoreWorkplaceTask(this, c.getPriority(), wt, StoreWorkplaceTask.Action.GET_PRICE));
            if (response instanceof StoreWorkplaceResponse) {
                cost += ((StoreWorkplaceResponse)response).getPrice() * needed.get(wt);
            }
            else {
                LOG.log(Level.WARNING, "{0} did not received a StoreWorkplaceResponse", this);
            }
            response = null;
        }
        solutions.add(new ConflictSolution(needed, cost));
        
        // solutions only for a contract, which is not of a LOW priority
        if (c.getPriority().isHigher(Priority.LOW)) {
            // replacing a contract in a Conveyor with the same product type
            List<Conveyor> lowerToReplace = conveyors.stream()
                    .filter(e -> e.getProductType() == c.getOrderedType())
                    .filter(e -> e.getPriority().isLess(c.getPriority()))
                    .sorted((Conveyor o1, Conveyor o2) -> {
                        return o1.getPriority().getIntValue() - o2.getPriority().getIntValue();
                    })
                    .collect(Collectors.toList());
            if (lowerToReplace.size() > 0) {
                solutions.add(new ConflictSolution(lowerToReplace.get(0), ConflictSolution.ConflictSolutionType.REPLACE_CONTRACT));
            }
            
            // decomposing a Conveyor with lower priority and with another product type
            List<Conveyor> lowerToDecompose = conveyors.stream()
                    .filter(e -> e.getProductType() != c.getOrderedType())
                    .filter(e -> e.getPriority().isLess(c.getPriority()))
                    .sorted((Conveyor o1, Conveyor o2) -> {
                        return o1.getPriority().getIntValue() - o2.getPriority().getIntValue();
                    })
                    .collect(Collectors.toList());
            Conveyor toDecompose = null;
            // finds one conveyor, which contains all needed Workplaces
            int neededInTotal = needed.keySet().stream().map((wt) -> needed.get(wt)).reduce(0, Integer::sum);
            for (Conveyor con : lowerToDecompose) {
                if (hasNeededWorkplaces(con, needed) == neededInTotal) {
                    toDecompose = con;
                    break;
                }
            }
            if (toDecompose != null) {
                solutions.add(new ConflictSolution(toDecompose, ConflictSolution.ConflictSolutionType.DECOMPOSE_CONVEYOR));
            }
        }
        return new ContractConflictTask(this, c, solutions);
    }

    /**
     * Decomposes a free Conveyor if it contains any needed Workplace.
     * 
     * @param c
     * @param found
     * @param needed 
     */
    private void addFromEmptyConveyors(Contract c, List<Workplace> found, Map<WorkplaceType, Integer> needed) {
        if (found.size() == c.getOrderedType().getConveyorWorkplaces().length) {
            return;
        }
        
        List<Conveyor> freeCon = conveyors.stream()
                .filter(e -> !e.hasContract())
                .collect(Collectors.toList());
        
        for (Conveyor con : freeCon) {
            if (hasNeededWorkplaces(con, needed) > 0) {
                List<Workplace> toAdd = con.decompose();
                conveyors.remove(con);
                factoryConfigurationArchive.deleteConveyor(con);
                for (Workplace workplace : toAdd) {
                    Integer count = needed.get(workplace.getWorkplaceType());
                    if (count != null && count > 0) {
                        found.add(workplace);
                        needed.put(workplace.getWorkplaceType(), count - 1);
                    }
                }
            }
        }
    }

    private void processConflictSolution(ConflictSolution cs, Contract contract, List<Workplace> found) {
        LOG.log(Level.FINEST, "User chose to {0}", cs);
        switch (cs.getType()) {
            case POSTPONE_CONTRACT:
                // Nothing to do now
                break;
            case REPLACE_CONTRACT:
                Conveyor toReplace = cs.getConveyor();
                if (toReplace != null) {
                    Contract removed = toReplace.removeContract();
                    if (removed != null) {
                        pendingContracts.add(removed);
                    }
                    if (toReplace.addContract(contract)) {
                        pendingContracts.remove(contract);
                    }
                    else {
                        LOG.warning("New Contract to replace the old one in Conveyor was not added");
                    }
                }
                break;
            case DECOMPOSE_CONVEYOR:
                pendingContracts.add(cs.getConveyor().removeContract());
                found.addAll(cs.getConveyor().decompose());
                conveyors.remove(cs.getConveyor());
                factoryConfigurationArchive.deleteConveyor(cs.getConveyor());
                createNewConveyor(found, contract);
                break;
            case BUY_WORKPLACES:
                List<WorkplaceType> toBuy = cs.getWorkplaces();
                for (WorkplaceType wt : toBuy) {
                    eventChannel.addTask(new StoreWorkplaceTask(this, contract.getPriority(), wt, StoreWorkplaceTask.Action.BUY));
                    if (response instanceof StoreWorkplaceResponse) {
                        StoreWorkplaceResponse swr = (StoreWorkplaceResponse)response;
                        if (swr.getWorkplace() != null) {
                            found.add(swr.getWorkplace());
                            // adds a new workplace into the Stock
                            eventChannel.addTask(new StockAddWorkplaceTask(this, contract.getPriority(), swr.getWorkplace()));
                        }
                    }
                    else {
                        LOG.log(Level.WARNING, "{0} did not received a StoreWorkplaceResponse", this);
                    }
                    response = null;
                }
                if (found.size() == contract.getOrderedType().getConveyorWorkplaces().length) {
                    createNewConveyor(found, contract);
                }
                else {
                    LOG.warning("Workplaces still missed after buying them");
                }
                break;
        }
    }

    private void processNewContractTask(NewContractTask nct) {
        eventChannel.addResponse(new NewContractResponse(this, nct, addContract(nct.getContract())));
    }
}
