
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.factory.Factory;
import cz.cvut.k36.omo.factory.contract.Priority;

/**
 * Task for a factory to get money from a user.
 */
public class MoneyNeededTask extends Task {

    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final int amount;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public MoneyNeededTask(EventChannelAccessible sender, Priority priority, int amount) {
        super(priority, sender, Factory.class);
        this.amount = amount;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */

    public int getAmount() {
        return amount;
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " to get " + amount + " of money from user";
    }
}
