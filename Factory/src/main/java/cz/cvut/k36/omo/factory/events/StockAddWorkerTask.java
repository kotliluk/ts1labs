
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.supply.Stock;
import cz.cvut.k36.omo.factory.worker.Worker;

/**
 * Task for a Stock to add a given Worker.
 */
public class StockAddWorkerTask extends Task {

    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final Worker worker;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public StockAddWorkerTask(EventChannelAccessible sender, Priority priority, Worker worker) {
        super(priority, sender, Stock.class);
        this.worker = worker;
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public Worker getWorker() {
        return worker;
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " from " + sender + " to add Worker " + worker;
    }
}