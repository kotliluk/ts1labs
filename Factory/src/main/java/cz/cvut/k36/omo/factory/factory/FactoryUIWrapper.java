package cz.cvut.k36.omo.factory.factory;

/**
 * User interface to communicate with Factory.
 */
public interface FactoryUIWrapper {
    /**
     * Starts communication with Factory. Sends commands to Factory
     */
    void run();
    /**
     * Receives a message from a factory.
     */
    void getMessage(String msg);
    /**
     * Receives a request from a Factory and sends answer.
     */
    String handleRequest(String request);
}
