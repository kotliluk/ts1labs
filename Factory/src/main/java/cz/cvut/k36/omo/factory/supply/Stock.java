
package cz.cvut.k36.omo.factory.supply;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.events.EventChannelAccessible;
import cz.cvut.k36.omo.factory.events.EventChannel;
import cz.cvut.k36.omo.factory.events.MoneyNeededResponse;
import cz.cvut.k36.omo.factory.events.MoneyNeededTask;
import cz.cvut.k36.omo.factory.events.StockResourceResponse;
import cz.cvut.k36.omo.factory.events.StockResourceTask;
import cz.cvut.k36.omo.factory.events.Response;
import cz.cvut.k36.omo.factory.events.StockAddWorkerTask;
import cz.cvut.k36.omo.factory.events.Task;
import cz.cvut.k36.omo.factory.events.StockGetWorkerTask;
import cz.cvut.k36.omo.factory.events.StockAddWorkplaceTask;
import cz.cvut.k36.omo.factory.events.StockGetWorkerResponse;
import cz.cvut.k36.omo.factory.events.StockGetWorkplaceResponse;
import cz.cvut.k36.omo.factory.events.StockGetWorkplaceTask;
import cz.cvut.k36.omo.factory.events.StoreResourceResponse;
import cz.cvut.k36.omo.factory.events.StoreResourceTask;
import cz.cvut.k36.omo.factory.worker.Worker;
import cz.cvut.k36.omo.factory.production.Workplace;
import cz.cvut.k36.omo.factory.production.WorkplaceType;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Stores all Factory's resources, Workplaces and Workers.
 */
public class Stock implements EventChannelAccessible {

    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static final Logger LOG = Logger.getLogger(Stock.class.getName());
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final EventChannel eventChannel;
    private Response response;
    
    private final Set<Worker> workers = new HashSet<>();
    private final Set<Workplace> workplaces = new HashSet<>();
    private final Map<ResourceType, Integer> resources = new HashMap<>();
    
    /* ----------------------- CONSTRUCTORS ----------------------- */

    public Stock(EventChannel eventChannel) {
        this.eventChannel = eventChannel;
        this.eventChannel.attach(this);
        for (ResourceType rt : ResourceType.values()) {
            resources.put(rt, 0);
        }
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */

    @Override
    public void processTask(Task task) {
        if (task instanceof StockAddWorkerTask) {
            processStockAddWorkerTask((StockAddWorkerTask)task);
        }
        else if (task instanceof StockGetWorkerTask) {
            processStockWorkerGetTask((StockGetWorkerTask)task);
        }
        else if (task instanceof StockResourceTask) {
            processResourceTask((StockResourceTask)task);
        }
        else if (task instanceof StockAddWorkplaceTask) {
            processStockWorkplaceAddTask((StockAddWorkplaceTask)task);
        }
        else if (task instanceof StockGetWorkplaceTask) {
            processStockWorkplaceGetTask((StockGetWorkplaceTask)task);
        }
        else {
            LOG.log(Level.WARNING, "{0} received an unexpected task: {1}", new Object[]{this, task});
        }
    }

    @Override
    public void receiveResponse(Response response) {
        this.response = response;
    }
    
    @Override
    public String toString() {
        return "Stock";
    }
    
    /* ----------------------- PRIVATE METHODS ----------------------- */
    
    /* ----------------------- WORKERS GETTERS ----------------------- */
    
    private Worker getFreeWorker() {
        Worker wor = workers.stream()
                .filter(w -> !w.isAttached())
                .findAny()
                .orElse(null);
        if (wor == null) {
            LOG.fine("There is not enough Workers in the factory");
            wor = new Worker();
            processStockAddWorkerTask(new StockAddWorkerTask(this, Priority.NORMAL, wor));
        }
        return wor;
    }
    
    private Worker getWorker(int ID) {
        return workers.stream()
            .filter(w -> w.getWorkerID() == ID)
            .findAny()
            .orElse(null);
    }
    
    /**
     * @return List of all Workers employed in the Factory
     */
    private List<Worker> getAllWorkers() {
        List<Worker> ret = new LinkedList<>();
        ret.addAll(workers);
        return ret;
    }
    
    /* ----------------------- WORKPLACES GETTERS ----------------------- */
    
    /**
     * Tries to find amount of free Workplaces of the given type and return them
     * in a list. If there is not enough Workplaces, all available Workplaces
     * are returned in a list.
     * 
     * @param wanted Wanted type of the Workplace
     * @return Free Workplace if found, null otherwise
     */
    private List<Workplace> getFreeWorkplaces(WorkplaceType wanted, int amount) {
        List<Workplace> all = workplaces.stream()
                .filter(w -> !w.isInConveyor())
                .filter(w -> w.getWorkplaceType() == wanted)
                .collect(Collectors.toList());
        if (all.size() < amount) {
            return all;
        }
        else {
            return all.subList(0, amount);
        }
    }
    
    /**
     * Returns a workplace of the given ID if there is any. Otherwise it returns
     * null.
     * 
     * @param ID ID of the wanted workplace
     * @return Workplace if found, null otherwise
     */
    private Workplace getWorkplace(int ID) {
        return workplaces.stream()
                .filter(w -> w.getWorkplaceID() == ID)
                .findAny()
                .orElse(null);
    }
    
    /**
     * @return List of all Workplaces in the Factory
     */
    private List<Workplace> getAllWorkplaces() {
        List<Workplace> ret = new LinkedList<>();
        ret.addAll(workplaces);
        return ret;
    }
    
    /* ----------------------- RESOURCE GETTERS ----------------------- */
    
    private int getResourceAmount(ResourceType type) {
        return resources.get(type);
    }
    
    private int useResource(ResourceType type, int amount) {
        int reserve = getResourceAmount(type) - amount;
        // if there is not enough resources in the Stock
        if (reserve < 0) {
            LOG.log(Level.INFO, "There is not enough {0} in the Stock: requested {1}, available {2}", new Object[]{type, amount, getResourceAmount(type)});
            if (type == ResourceType.MONEY) {
                eventChannel.addTask(new MoneyNeededTask(this, Priority.NORMAL, reserve * -1));
                if (response instanceof MoneyNeededResponse) {
                    addResource(type, ((MoneyNeededResponse)response).getAmount());
                }
                else {
                    LOG.log(Level.WARNING, "{0} did not received a MoneyNeededResponse", this);
                }
            }
            else {
                eventChannel.addTask(new StoreResourceTask(this, Priority.NORMAL, type, roundUp(amount, 1000)));
                if (response instanceof StoreResourceResponse) {
                    addResource(type, ((StoreResourceResponse)response).getResource());
                }
                else {
                    LOG.log(Level.WARNING, "{0} did not received a StoreResourceResponse", this);
                }
            }
            response = null;
            return useResource(type, amount);
        }
        else {
            LOG.log(Level.FINE, "There is enough {0} in the Stock: requested {1}, available {2}", new Object[]{type, amount, getResourceAmount(type)});
            changeResourceAmount(type, amount * -1);
            return amount;
        }
    }
    
    private void addResource(ResourceType type, int amount) {
        if (amount > 0) {
            changeResourceAmount(type, amount);
        }
    }

    private void changeResourceAmount(ResourceType type, int amount) {
        int total = resources.get(type);
        total += amount;
        resources.put(type, total);
    }
    
    private int roundUp(int toRound, int toWhat) {
        int ret = ((toRound / toWhat) + 1) * toWhat;
        return ret;
    }

    /* ----------------------- PROCESSING TASKS ----------------------- */

    private void processStockAddWorkerTask(StockAddWorkerTask sawt) {
        if (sawt.getWorker() != null) {
            workers.add(sawt.getWorker());
            LOG.log(Level.FINE, "A Worker {0} was added into the Stock", sawt.getWorker());
        }
    }
    
    private void processStockWorkerGetTask(StockGetWorkerTask swgt) {
        List<Worker> ret = new LinkedList<>();
        switch (swgt.getWorkerIdentification()) {
            case ALL:
                ret.addAll(getAllWorkers());
                break;
            case ID:
                ret.add(getWorker(swgt.getWorkerID()));
                break;
            case FREE:
                ret.add(getFreeWorker());
                break;
        }
        eventChannel.addResponse(new StockGetWorkerResponse(this, swgt, ret));
    }

    private void processResourceTask(StockResourceTask sgrt) {
        switch(sgrt.getActionType()) {
            case ADD:
                LOG.log(Level.FINE, "{0} of {1} added to Stock", new Object[]{sgrt.getAmount(), sgrt.getResourceType()});
                addResource(sgrt.getResourceType(), sgrt.getAmount());
                break;
            case USE:
                LOG.log(Level.FINE, "{0} of {1} from Stock is asked to be used", new Object[]{sgrt.getAmount(), sgrt.getResourceType()});
                eventChannel.addResponse(new StockResourceResponse(this, sgrt, useResource(sgrt.getResourceType(), sgrt.getAmount())));
                break;
            case GET_AMOUNT:
                LOG.log(Level.FINEST, "Stock was asked for the amount of {0}", sgrt.getResourceType());
                eventChannel.addResponse(new StockResourceResponse(this, sgrt, getResourceAmount(sgrt.getResourceType())));
                break;
        }
    }

    private void processStockWorkplaceAddTask(StockAddWorkplaceTask sawt) {
        if (sawt.getWorkplace() != null) {
            workplaces.add(sawt.getWorkplace());
        }
    }

    private void processStockWorkplaceGetTask(StockGetWorkplaceTask swgt) {
        List<Workplace> ret = new LinkedList<>();
        switch(swgt.getWorkplaceIdentification()) {
            case ALL:
                ret.addAll(getAllWorkplaces());
                break;
            case ID:
                Workplace found = getWorkplace(swgt.getWorkplaceID());
                if (found != null) {
                    ret.add(found);
                }
                break;
            case FREE:
                ret.addAll(getFreeWorkplaces(swgt.getWorkplaceType(), swgt.getAmount()));
                break;
        }
        eventChannel.addResponse(new StockGetWorkplaceResponse(this, swgt, ret));
    }
}
