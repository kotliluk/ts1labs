
package cz.cvut.k36.omo.factory.factory;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.events.EventChannel;
import cz.cvut.k36.omo.factory.events.EventChannelAccessible;
import cz.cvut.k36.omo.factory.events.Response;
import cz.cvut.k36.omo.factory.events.StockAddWorkerTask;
import cz.cvut.k36.omo.factory.events.StockResourceTask;
import cz.cvut.k36.omo.factory.events.StockAddWorkplaceTask;
import cz.cvut.k36.omo.factory.events.Task;
import cz.cvut.k36.omo.factory.production.Production;
import cz.cvut.k36.omo.factory.supply.ResourceType;
import cz.cvut.k36.omo.factory.supply.Stock;
import cz.cvut.k36.omo.factory.supply.Store;
import cz.cvut.k36.omo.factory.worker.Accounting;
import cz.cvut.k36.omo.factory.worker.Worker;
import cz.cvut.k36.omo.factory.production.Machine;
import cz.cvut.k36.omo.factory.production.MachineType;
import cz.cvut.k36.omo.factory.production.Robot;
import cz.cvut.k36.omo.factory.production.RobotType;
import cz.cvut.k36.omo.factory.production.WorkplaceType;
import java.util.HashMap;
import java.util.Map;

/**
 * Factory class builder.
 */
public final class FactoryBuilder {
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final EventChannelAccessible initEventAccess;
    
    private EventChannel eventChannel;
    private Factory factory;
    private Stock stock;
    private Store store;
    private Production production;
    // to be set just before creating a Factory
    private int workerWages;
    private float workplaceRatio;
    private float resourceRatio;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */

    public FactoryBuilder() {
        initEventAccess = new EventChannelAccessible() {
            @Override
            public void processTask(Task task) { /* does nothing */ }

            @Override
            public void receiveResponse(Response response) { /* does nothing */ }
            
            @Override
            public String toString() { return "FactoryBuilder"; }
        };
        createInstances();
        this.workerWages = 10;
        this.workplaceRatio = 1.0f;
        this.resourceRatio = 1.0f;
    }
    
    /* ----------------------- PUBLIC STATIC METHODS ----------------------- */
    
    /**
     * Creates a default Factory. The factory contains:
     * - 4 Workers
     * - each type of the Workplace once
     * - 100 000 money
     * - 10 000 steal
     * - 1 000 of all other Resources
     * Worker wages are 10 per tact.
     * 
     * @return FactoryBuilder holding default Factory instance
     */
    public static FactoryBuilder createDefaultFactory() {
        FactoryBuilder builder = new FactoryBuilder();
        builder.addWorkers(4);
        for (WorkplaceType wt : RobotType.values()) {
            builder.addWorkplace(wt, 1);
        }
        for (WorkplaceType wt : MachineType.values()) {
            builder.addWorkplace(wt, 1);
        }
        for (ResourceType rt : ResourceType.values()) {
            builder.addResource(rt, 1000);
        }
        builder.addResource(ResourceType.MONEY, 99000);
        builder.addResource(ResourceType.STEAL, 9000);
        return builder;
    }
    
    /**
     * Creates a default Factory. The factory contains:
     * - 0 Workers
     * - no Workplaces
     * - 0 money
     * - 0 of all Resources
     * Worker wages are 10 per tact.
     * 
     * @return FactoryBuilder holding empty Factory instance
     */
    public static FactoryBuilder createEmptyFactory() {
        return new FactoryBuilder();
    }
    
    /**
     * Creates a default Factory. The factory contains:
     * - 20 Workers
     * - each type of the Workplace five times
     * - 100 000 money
     * - 50 000 steal
     * - 5 000 of all Resources
     * Worker wages are 10 per tact.
     * 
     * @return FactoryBuilder holding empty Factory instance
     */
    public static FactoryBuilder createFullFactory() {
        FactoryBuilder builder = new FactoryBuilder();
        builder.addWorkers(20);
        for (WorkplaceType wt : RobotType.values()) {
            builder.addWorkplace(wt, 5);
        }
        for (WorkplaceType wt : MachineType.values()) {
            builder.addWorkplace(wt, 5);
        }
        for (ResourceType rt : ResourceType.values()) {
            builder.addResource(rt, 5000);
        }
        builder.addResource(ResourceType.MONEY, 95000);
        builder.addResource(ResourceType.STEAL, 45000);
        return builder;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    /**
     * Inits prices of Workplaces in the Store with the given workplaceRation and
     * prices of Resources in the Store with the given resourceRatio.
     * Given parameteres must be positive, otherwise IllegalArgumentException
     * is thrown.
     * After this call, Store.getWorkplacePrice(wt) == (int)Math.ceil(wt.getBasePrice() * workplaceRatio),
     * analogously with ResourceTypes.
     * The price is never set to zero, it is rounded up.
     * 
     * @param workplaceRatio ratio to multiply Workplace base price
     * @param resourceRatio ratio to multiply Resource base price
     * @return FactoryBuilder instance, which the function was called on
     * @throws IllegalArgumentException Thrown when workplaceRatio or resourceRatio
     * is less or equals to zero
     */
    public FactoryBuilder setStorePrices(float workplaceRatio, float resourceRatio) throws IllegalArgumentException {
        if (workplaceRatio <= 0 || resourceRatio <= 0) {
            throw new IllegalArgumentException();
        }
        this.workplaceRatio = workplaceRatio;
        this.resourceRatio = resourceRatio;
        return this;
    }
    
    /**
     * Addes given number of Workers into the Factory's Stock.
     * 
     * @param number number of Workers to be added
     * @return FactoryBuilder instance, which the function was called on
     */
    public FactoryBuilder addWorkers(int number) {
        for (int i = 0; i < number; ++i) {
            eventChannel.addTask(new StockAddWorkerTask(initEventAccess, Priority.NORMAL, new Worker()));
        }
        return this;
    }
    
    /**
     * Sets Employees' wages used in the Factory. Given values must be positive
     * integers. Default values are workerWages = 10.
     * 
     * @param workerWages wages used for workers
     * @return FactoryBuilder instance, which the function was called on
     */
    public FactoryBuilder setWages(int workerWages) {
        if (workerWages > 0) {
            this.workerWages = workerWages;
        }
        return this;
    }
    
    /**
     * Adds given number of the given type of Workplace into the Factory's
     * Stock.
     * 
     * @param wt Type of the Workplace to be added
     * @param number number of Workplaces to be added
     * @return FactoryBuilder instance, which the function was called on
     */
    public FactoryBuilder addWorkplace(WorkplaceType wt, int number) {
        if (wt.isRobotType()) {
            for (int i = 0; i < number; ++i) {
                eventChannel.addTask(new StockAddWorkplaceTask(initEventAccess, Priority.NORMAL, new Robot((RobotType)wt)));
            }
        }
        else {
            for (int i = 0; i < number; ++i) {
                eventChannel.addTask(new StockAddWorkplaceTask(initEventAccess, Priority.NORMAL, new Machine((MachineType)wt)));
            }
        }
        return this;
    }
    
    /**
     * Addes given number of the given type of Resource into the Factory's
     * Stock.
     * 
     * @param rt Type of the Resource to be added
     * @param number number of Resources to be added
     * @return FactoryBuilder instance, which the function was called on
     */
    public FactoryBuilder addResource(ResourceType rt, int number) {
        eventChannel.addTask(new StockResourceTask(initEventAccess, Priority.NORMAL, rt, StockResourceTask.Action.ADD, number));
        return this;
    }
    
    /**
     * Returns built Factory from the FactoryBuilder. Calling this just after
     * creating FactoryBuilder returns a valid Factory, with 0 Workers,
     * 0 Workplaces, 0 Resources and with Store with default prices.
     * @return Built Factory
     */
    public Factory getFactory() {
        Map<WorkplaceType, Integer> wtp = new HashMap<>();
        Map<ResourceType, Integer> rtp = new HashMap<>();
        for (WorkplaceType wt : MachineType.values()) {
            wtp.put(wt, (int)Math.ceil(wt.getBasePrice() * workplaceRatio));
        }
        for (WorkplaceType wt : RobotType.values()) {
            wtp.put(wt, (int)Math.ceil(wt.getBasePrice() * workplaceRatio));
        }
        for (ResourceType rt : ResourceType.values()) {
            rtp.put(rt, (int)Math.ceil(rt.getBasePrice() * resourceRatio));
        }
        this.store = new Store(eventChannel, wtp, rtp);
        this.production = new Production(eventChannel, new Accounting(workerWages));
        this.factory = new Factory(production, eventChannel);
        return factory;
    }
    
    /* ----------------------- PRIVATE METHODS ----------------------- */

    private void createInstances() {
        this.eventChannel = new EventChannel();
        this.stock = new Stock(eventChannel);
    }
}
