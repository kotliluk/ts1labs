
package cz.cvut.k36.omo.factory.product;

import cz.cvut.k36.omo.factory.production.Workplace;

/**
 * Product created in the Factory.
 */
public class Product {
    
    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static int productCount = 0;

    public static int existingProducts() {
        return productCount;
    }
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final int productID;
    private final ProductType type;
    private int processingStage = 0;
    private boolean sold = false;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public Product(ProductType type) {
        this.productID = ++productCount;
        this.type = type;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public ProductType getType() {
        return type;
    }
    
    /**
     * Processes Product into next stage of production. If the given Workplace
     * is not the one, which is needed at the moment for the Product, no changes
     * are made and false is returned.
     * 
     * @param workplace Workplace processing the Product
     * @return True if the Product was really processed, false otherwise
     */
    public boolean beProcessedBy(Workplace workplace) {
        if (isDone()) {
            return false;
        }
        if (workplace.getWorkplaceType() == type.getConveyorWorkplaces()[processingStage]) {
            ++processingStage;
            return true;
        }
        return false;
    }
    
    /**
     * @return True if the Product is fully created, false otherwise
     */
    public boolean isDone() {
        return processingStage == type.getConveyorWorkplaces().length;
    }
    
    /**
     * Set the Product as sold to control only one selling of the Product.
     * 
     * @return True if this was the firts selling of the Product (isSold() on
     * this Product before returned false), false if the Product was already
     * sold before
     */
    public boolean setSold() {
        if (sold) {
            return false;
        }
        sold = true;
        return true;
    }

    /**
     * @return True if the Product was already sold (added to a Contract),
     * false otherwise
     */
    public boolean isSold() {
        return sold;
    }
    
    @Override
    public String toString() {
        return "Product " + type + "-" + productID;
    }
}
