
package cz.cvut.k36.omo.factory.archive;

import cz.cvut.k36.omo.factory.factory.Factory;
import cz.cvut.k36.omo.factory.production.Conveyor;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FactoryConfigurationArchive implements SpecialArchive {
    
    public static class HistoryConveyor {
        int start;
        int end;
        String conveyorStr;
        String workplaceStr;
        Conveyor conveyor;

        public HistoryConveyor(int start, Conveyor conveyor) {
            this.start = start;
            this.end = Integer.MAX_VALUE;
            this.conveyorStr = "Conveyor " + conveyor.getConveyorId() + " for " + conveyor.getCurrentContract().getOrderedType();
            this.workplaceStr = conveyor.getWorkplaceStr();
            this.conveyor = conveyor;
        }

        public Conveyor getConveyor() {
            return conveyor;
        }
    }
    
    /* ----------------------- INSTANCE'S FIELDS ----------------------- */
    
    private final List<HistoryConveyor> config;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    FactoryConfigurationArchive(){
        config = new ArrayList<>();
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public List<HistoryConveyor> getConfig(){
        return config;
    }
    
    /**
     * Returns all Conveyors, which existed at least 1 tact in tacts [start; end].
     * 
     * @param start start of the period
     * @param end end of the period
     * @return conveyors, which existed in the period
     */
    public List<HistoryConveyor> getConfig(int start, int end){
        return config.stream()
                .filter(p -> !(p.start > end || p.end < start))
                .collect(Collectors.toList());
    }

    public void addConveyor(Conveyor conveyor){
        if (config.stream()
                .map(p -> p.conveyor)
                .collect(Collectors.toList())
                .contains(conveyor)) {
            deleteConveyor(conveyor);
        }
        config.add(new HistoryConveyor(Factory.getCurrentTact(), conveyor));
    }

    public boolean deleteConveyor(Conveyor conveyor){
        if (!config.stream()
                .map(p -> p.conveyor)
                .collect(Collectors.toCollection(ArrayList::new))
                .contains(conveyor)){
            return false;
        }else{
           HistoryConveyor tmp = config.stream()
                   .filter(p -> p.conveyor == conveyor)
                   .findFirst()
                   .orElse(null);
            if (tmp != null) {
                tmp.conveyor = null;
                tmp.end = Factory.getCurrentTact();
            }
            return true;
        }
    }

    @Override
    public ArchiveType getType() {
        return ArchiveType.CONFIGURATION;
    }
}
