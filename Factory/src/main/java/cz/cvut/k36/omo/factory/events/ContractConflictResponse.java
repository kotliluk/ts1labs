
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.ConflictSolution;

/**
 * Response to ContractConflictTask holding choosen ConflictSolution from the user.
 */
public class ContractConflictResponse extends Response {

    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final ConflictSolution solution;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public ContractConflictResponse(EventChannelAccessible sender, ContractConflictTask previousTask, ConflictSolution solution) {
        super(sender, previousTask);
        this.solution = solution;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public ConflictSolution getSolution() {
        return solution;
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " returning choosen solution: " + solution;
    }
}
