
package cz.cvut.k36.omo.factory.product;

import cz.cvut.k36.omo.factory.production.MachineType;
import cz.cvut.k36.omo.factory.production.RobotType;
import cz.cvut.k36.omo.factory.production.WorkplaceType;

/**
 * Type of the Product and Product's general characteristics.
 */
public enum ProductType {
    CAR(100, new WorkplaceType[] { MachineType.PRESS_SHOP, MachineType.ENGINE_PLANT, MachineType.INTERIOR_SETTING,
        MachineType.ASSEMBLY_SHOP, RobotType.WHEEL_MOUNTING, RobotType.COLORING }),
    MOTORBIKE(20, new WorkplaceType[] { MachineType.ENGINE_PLANT, MachineType.ASSEMBLY_SHOP, RobotType.COLORING }),
    TRUCK(1200, new WorkplaceType[] { MachineType.PRESS_SHOP, MachineType.PRESS_SHOP, MachineType.WELDING_SHOP,
        MachineType.WELDING_SHOP, MachineType.ENGINE_PLANT, MachineType.ASSEMBLY_SHOP, MachineType.ASSEMBLY_SHOP,
        RobotType.WHEEL_MOUNTING, RobotType.WHEEL_MOUNTING, RobotType.COLORING }),
    BUS(1000, new WorkplaceType[] {MachineType.PRESS_SHOP, MachineType.PRESS_SHOP, MachineType.WELDING_SHOP,
        MachineType.ENGINE_PLANT, MachineType.ASSEMBLY_SHOP, MachineType.INTERIOR_SETTING, MachineType.INTERIOR_SETTING, 
        RobotType.GLASS_MOUNTING, RobotType.GLASS_MOUNTING, RobotType.COLORING }),
    CARAVAN(300, new WorkplaceType[] { MachineType.PRESS_SHOP, MachineType.ENGINE_PLANT, MachineType.ASSEMBLY_SHOP, 
        MachineType.INTERIOR_SETTING, MachineType.INTERIOR_SETTING, RobotType.GLASS_MOUNTING, RobotType.WHEEL_MOUNTING,
        RobotType.COLORING });
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final int stealNeeded;
    private final WorkplaceType[] conveyorWorkplaces;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    private ProductType(int stealNeeded, WorkplaceType[] conveyorWorkplaces) {
        this.stealNeeded = stealNeeded;
        this.conveyorWorkplaces = conveyorWorkplaces;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    /**
     * @return Amount of steal needed for initial creation of the Product
     */
    public int getStealNeeded() {
        return stealNeeded;
    }

    /**
     * @return Sequence of Workplaces to process the Product
     */
    public WorkplaceType[] getConveyorWorkplaces() {
        return conveyorWorkplaces;
    }
}
