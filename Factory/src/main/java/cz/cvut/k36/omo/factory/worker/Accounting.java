
package cz.cvut.k36.omo.factory.worker;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Pays off Workers' wages.
 */
public class Accounting {

    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static final Logger LOG = Logger.getLogger(Accounting.class.getName());
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final int workerWages;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public Accounting(int workerWages) {
        this.workerWages = workerWages;
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */

    /**
     * Pays wages of given Employees.
     * 
     * @param employees employees to receive their wages
     * @return total money used to pay the wages
     */
    public int payWorkerWages(List<Worker> employees) {
        int paidInTotal = 0;
        for (Employee e : employees) {
            int payed = payEmployeeWages(e);
            LOG.log(Level.FINEST, "For {0} payed {1}", new Object[]{e, payed});
            paidInTotal += payed;
        }
        return paidInTotal;
    }

    public int getWorkerWages() {
        return workerWages;
    }
    
    @Override
    public String toString() {
        return "Accounting";
    }
    
    /* ----------------------- PRIVATE METHODS ----------------------- */

    private int payEmployeeWages(Employee e) {
        int toPay = e.getHoursToBilling() * workerWages;
        e.earnMoney(toPay);
        return toPay;
    }
}
