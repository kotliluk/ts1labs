
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.factory.Factory;
import cz.cvut.k36.omo.factory.contract.Priority;

/**
 * Task for a Factory to display given message to user.
 */
public class MessageToUserTask extends Task {
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final String message;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public MessageToUserTask(EventChannelAccessible sender, Priority priority, String message) {
        super(priority, sender, Factory.class);
        this.message = message;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public String getMessage() {
        return message;
    }
}
