
package cz.cvut.k36.omo.factory.production;

/**
 * Type of Machine and its general characteristics.
 */
public enum MachineType implements WorkplaceType {
    ASSEMBLY_SHOP(3000, 3, 0, "Assembly shop"),
    PRESS_SHOP(2500, 3, 2, "Press shop"),
    ENGINE_PLANT(2000, 2, 2, "Engine plant"),
    INTERIOR_SETTING(1500, 2, 0, "Interior setting"),
    WELDING_SHOP(2000, 3, 0, "Welding shop");
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final int price;
    private final int electricityConsumption;
    private final int oilConsumption;
    private final String typeString;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    private MachineType(int price, int electricityConsumption, 
                        int oilConsumption, String typeString) {
        this.price = price;
        this.electricityConsumption = electricityConsumption;
        this.oilConsumption = oilConsumption;
        this.typeString = typeString;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    @Override
    public int getBasePrice() {
        return price;
    }
    
    @Override
    public int getElectricityConsumption() {
        return electricityConsumption;
    }

    @Override
    public int getOilConsumption() {
        return oilConsumption;
    }

    @Override
    public String toString() {
        return typeString;
    }

    @Override
    public boolean isMachineType() {
        return true;
    }

    @Override
    public boolean isRobotType() {
        return false;
    }
}
