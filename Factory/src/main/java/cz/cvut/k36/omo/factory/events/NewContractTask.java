
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Contract;
import cz.cvut.k36.omo.factory.production.Production;

/**
 * Task for a Production to add a new Contract.
 */
public class NewContractTask extends Task {

    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final Contract contract;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */

    public NewContractTask(EventChannelAccessible sender, Contract contract) {
        super(contract.getPriority(), sender, Production.class);
        this.contract = contract;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public Contract getContract() {
        return contract;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " to add " + contract;
    }
}
