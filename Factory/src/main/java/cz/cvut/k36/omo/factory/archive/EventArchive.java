
package cz.cvut.k36.omo.factory.archive;

import cz.cvut.k36.omo.factory.factory.Factory;
import cz.cvut.k36.omo.factory.events.Response;
import cz.cvut.k36.omo.factory.events.Task;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class EventArchive implements SpecialArchive {
    
    abstract class ReportEvent {
        int tact;
        
        ReportEvent(int tact) {
            this.tact = tact;
        }
        
        abstract boolean isReportResponse();
        abstract boolean isReportTask();
    }
    
    class ReportResponse extends ReportEvent {
        Response r;
        public ReportResponse(int tact, Response r) {
            super(tact);
            this.r = r;
        }
        
        @Override
        public String toString() {
            return "Tact" + tact + ": " + r;
        }

        @Override
        public boolean isReportResponse() {
            return true;
        }

        @Override
        public boolean isReportTask() {
            return false;
        }
    }
    
    class ReportTask extends ReportEvent {
        Task e;
        public ReportTask(int tact, Task e) {
            super(tact);
            this.e = e;
        }
        
        @Override
        public String toString() {
            return "Tact" + tact + ": " + e;
        }

        @Override
        public boolean isReportResponse() {
            return false;
        }

        @Override
        public boolean isReportTask() {
            return true;
        }
    }
    
    /* ----------------------- INSTANCE'S FIELDS ----------------------- */
    
    private final List<ReportEvent> eventChannelHistory;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    EventArchive(){
        eventChannelHistory = new LinkedList<>();
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public void addHistoryTask(Task task){
        eventChannelHistory.add(new ReportTask(Factory.getCurrentTact(), task));
    }

    public void addHistoryResponse(Response response){
        eventChannelHistory.add(new ReportResponse(Factory.getCurrentTact(), response));
    }
    
    public List<ReportEvent> getEvents() {
        return eventChannelHistory;
    }
    
    public List<ReportEvent> getEvents(int start, int end) {
        return eventChannelHistory.stream()
                .filter(p -> p.tact >= start)
                .filter(p -> p.tact <= end)
                .collect(Collectors.toList());
    }

    public List<ReportTask> getTasks() {
        return eventChannelHistory.stream()
                .filter(p -> p.isReportTask())
                .map(p -> (ReportTask)p)
                .collect(Collectors.toList());
    }

    public List<ReportTask> getTasks(int start, int end) {
        return eventChannelHistory.stream()
                .filter(p -> p.isReportResponse())
                .map(p -> (ReportTask)p)
                .filter(p -> p.tact >= start)
                .filter(p -> p.tact <= end)
                .collect(Collectors.toList());
    }

    public List<ReportResponse> getResponses() {
        return eventChannelHistory.stream()
                .filter(p -> p instanceof ReportResponse)
                .map(p -> (ReportResponse)p)
                .collect(Collectors.toList());
    }

    public List<ReportResponse> getResponses(int start, int end) {
        return eventChannelHistory.stream()
                .filter(p -> p instanceof ReportResponse)
                .map(p -> (ReportResponse)p)
                .filter(p -> p.tact >= start)
                .filter(p -> p.tact <= end)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public ArchiveType getType() {
        return ArchiveType.EVENT;
    }
}
