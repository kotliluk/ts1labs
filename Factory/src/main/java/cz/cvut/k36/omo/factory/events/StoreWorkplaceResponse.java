
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.production.Workplace;

/**
 * Response to a StoreWorkplaceTask holding received Workplace and its price.
 */
public class StoreWorkplaceResponse extends Response {

    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final Workplace workplace;
    private final int price;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public StoreWorkplaceResponse(EventChannelAccessible sender, StoreWorkplaceTask previousTask, Workplace workplace, int price) {
        super(sender, previousTask);
        this.workplace = workplace;
        this.price = price;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    /**
     * 
     * @return Workplace if the purchase succeeded, null if failed or if the task for only price getting
     */
    public Workplace getWorkplace() {
        return workplace;
    }

    public int getPrice() {
        return price;
    }
    
    @Override
    public String toString() {
        if (workplace != null) {
            return this.getClass().getSimpleName() + " returning " + workplace + " from " + sender + " to " + previousTask.sender + " for price " + price;
        }
        return this.getClass().getSimpleName() + " returning " + ((StoreWorkplaceTask)previousTask).getWorkplaceType() + "'s price: " + price;
    }
}
