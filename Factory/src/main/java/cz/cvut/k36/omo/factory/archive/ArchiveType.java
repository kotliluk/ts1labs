package cz.cvut.k36.omo.factory.archive;

public enum ArchiveType {
    EVENT,
    CONSUMPTION,
    CONFIGURATION
}
