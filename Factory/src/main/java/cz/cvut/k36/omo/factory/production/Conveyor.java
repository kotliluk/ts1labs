
package cz.cvut.k36.omo.factory.production;

import cz.cvut.k36.omo.factory.contract.Contract;
import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.events.EventChannelAccessible;
import cz.cvut.k36.omo.factory.events.EventChannel;
import cz.cvut.k36.omo.factory.events.MessageToUserTask;
import cz.cvut.k36.omo.factory.events.StockResourceTask;
import cz.cvut.k36.omo.factory.events.Response;
import cz.cvut.k36.omo.factory.events.StockGetWorkerResponse;
import cz.cvut.k36.omo.factory.events.StockGetWorkerTask;
import cz.cvut.k36.omo.factory.events.Task;
import cz.cvut.k36.omo.factory.events.StockResourceResponse;
import cz.cvut.k36.omo.factory.product.Product;
import cz.cvut.k36.omo.factory.product.ProductType;
import cz.cvut.k36.omo.factory.supply.ResourceType;
import cz.cvut.k36.omo.factory.worker.Worker;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Conveyor class controls processing of the product in WorkplaceHolders.
 */
public class Conveyor implements EventChannelAccessible {

    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static final Logger LOG = Logger.getLogger(Conveyor.class.getName());
    private static int ID = 0;
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final EventChannel eventChannel;
    private Response response;
    
    private final Production production;
    private final ProductType productType;
    private final WorkplaceHolder[] holders;
    private final int totalHolders;

    private final int conveyorId;
    private String workplaceStr = "";
    private Contract currentContract;
    private int productsInProcess = 0;
    private boolean decomposed;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    Conveyor(Production production, Contract contract, EventChannel eventChannel, List<Workplace> workplaces) throws IllegalArgumentException {
        this.eventChannel = eventChannel;
        this.eventChannel.attach(this);
        this.production = production;
        this.currentContract = contract;
        this.productType = contract.getOrderedType();
        this.totalHolders = productType.getConveyorWorkplaces().length;
        this.holders = new WorkplaceHolder[totalHolders];
        this.conveyorId = ++ID;
        if (!addWorkplaces(workplaces)) {
            throw new IllegalArgumentException();
        }
        this.decomposed = false;
        workplaces.forEach(w -> workplaceStr += "\t" + w + "\n");
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */

    /**
     * @return Priority of the current Contract in the Conveyor. If there is no
     * Contract at the moment, Priority.NO is returned.
     */
    public Priority getPriority() {
        if (hasContract()) {
            return currentContract.getPriority();
        }
        return Priority.NO;
    }
    
    /**
     * Returns current Contract in the Conveyor. The Contract remains added to
     * to the Conveyor after calling this method.
     * 
     * @return Current Contract in the Conveyor, null if there is no Contract
     * at the moment
     */
    public Contract getCurrentContract() {
        return currentContract;
    }

    @Override
    public void processTask(Task task) {
        LOG.log(Level.WARNING, "{0} received an unexpected task: {1}", new Object[]{this, task});
    }

    @Override
    public void receiveResponse(Response response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "Conveyor for product " + productType + " with current contract \"" + currentContract + '\"';
    }
    
    /* ----------------------- PACKAGE METHODS ----------------------- */

    /**
     * Adds given contract to the Conveyor, if there is no contract added yet
     * and if the given Contract orders the same type of Product as the Conveyor
     * processes. Use Conveyor.removeContract to ensure it succeeds.
     * 
     * @param contract Contract to be added
     * @return True, if added successfully, false otherwise
     */
    boolean addContract(Contract contract) {
        if (currentContract == null && contract != null && contract.getOrderedType() == productType) {
            currentContract = contract;
            return true;
        }
        return false;
    }

    public int getConveyorId() {
        return conveyorId;
    }

    public String getWorkplaceStr() {
        return workplaceStr;
    }

    /**
     * Sets Conveyor's current Contract to null and returns the previous one.
     * All not-finished products in the Conveyor are destoyed (their initial
     * used resources (steal) are given back into the Stock.
     * 
     * @return Previous Contract in the Conveyor, null if there was no Contract
     * before
     */
    Contract removeContract() {
        clearWorkplaces();
        Contract ret = currentContract;
        currentContract = null;
        return ret;
    }

    boolean isDecomposed() {
        return decomposed;
    }
    
    /**
     * @return True if the Conveyor has a Contract, false otherwise
     */
    boolean hasContract() {
        return currentContract != null;
    }
    
    /**
     * Decomposes the Conveyor, if it has no Contract at the moment and it was
     * not decomposed before. Sets all Conveyor's Workplaces as free.
     * Detaches the Conveyor from the EventChannel. After calling this, the
     * Conveyor should be never used again.
     * 
     * @return List of (freed) Workplace which were used in the Conveyor. List is
     * empty, if the decomposing did not succeed
     */
    List<Workplace> decompose() {
        List<Workplace> ret = new LinkedList<>();
        if (hasContract() || decomposed) {
            return ret;
        }
        for (WorkplaceHolder wh : holders) {
            wh.getWorkplace().addToConveyor(null);
            ret.add(wh.getWorkplace());
            wh = null;
        }
        this.eventChannel.detach(this);
        this.decomposed = true;
        return ret;
    }
    
    /**
     * Does next tact if all Workplaces are ready and any Contract is added to
     * the Conveyor. WorkplaceHolders in the Conveyor pass next series of Products.
     */
    void process() {
        if (prepare()) {
            if (!hasContract()) {
                LOG.log(Level.INFO, "Conveyor for {0} does not have a contract", productType.toString());
                return;
            }
            Product newProduct = null;
            if (productsInProcess + currentContract.getFinishedCount() < currentContract.getOrderedCount()) {
                int steal = 0;
                if (productType.getStealNeeded() > 0) {
                    LOG.log(Level.FINEST, "{0} needs {1} for a new product{2}", new Object[]{this, productType.getStealNeeded(), productType});
                    eventChannel.addTask(new StockResourceTask(this, currentContract.getPriority(), ResourceType.STEAL, StockResourceTask.Action.USE, productType.getStealNeeded()));
                    if (response instanceof StockResourceResponse) {
                        steal = ((StockResourceResponse)response).getResponseAmount();
                        LOG.log(Level.FINEST, "{0} received {1} of STEAL for the Workplace", new Object[]{this, steal});
                    }
                    else {
                        LOG.log(Level.WARNING, "{0} did not received a StockResourceResponse", this);
                    }
                    response = null;
                }
                if (steal == productType.getStealNeeded()) {
                    newProduct = new Product(productType);
                    ++productsInProcess;
                }
                else {
                    LOG.log(Level.WARNING, "{0} did not receive enough STEAL for a new Product", this);
                }
            }
            
            Product result = holders[0].runWorkplace(newProduct);
            if (result != null) {
                eventChannel.addTask(new MessageToUserTask(this, currentContract.getPriority(), result + " from " + currentContract + " finished"));
                currentContract.addFinishedProduct(result);
                --productsInProcess;
                if (currentContract.isFinished()) {
                    production.contractDone(removeContract());
                }
            }
        }
        else {
            LOG.log(Level.INFO, "Conveyor for {0} does not have ready workplaces", productType.toString());
        }
    }

    /**
     * @return Type of the Product currently being processed in the Conveyor, in
     * other words type of the Product ordered in currently added Contract
     */
    ProductType getProductType() {
        return productType;
    }

    /**
     * Gets given amount of the resource from the Stock.
     *
     * @param resource Type of the wanted Resource
     * @param amount Amount of the resource
     * @return Amount of the resource got from the Stock, return == amount if
     * succeeded, 0 if the Stock did not give it
     */
    int useResources(ResourceType resource, int amount) {
        int got = 0;
        eventChannel.addTask(new StockResourceTask(this, currentContract.getPriority(), resource, StockResourceTask.Action.USE, amount));
        if (response instanceof StockResourceResponse) {
            got = ((StockResourceResponse)response).getResponseAmount();
            LOG.log(Level.FINEST, "{0} received {1} of {2} for the Workplace", new Object[]{this, got, resource});
        }
        else {
            LOG.log(Level.WARNING, "{0} did not received a StockResourceResponse", this);
        }
        response = null;
        return got;
    } 

    /* ----------------------- PRIVATE METHODS ----------------------- */

    private boolean addWorkplaces(List<Workplace> workplaces) {
        int holdersMounted = 0;
        for (Workplace w : workplaces) {
            if (addWorkplace(w)) {
                ++holdersMounted;
            }
        }
        if (holdersMounted == totalHolders) {
            connectHolders();
            return true;
        }
        return false;
    }

    /**
     * Tries to add given workplace in the conveyor. If there is an empty
     * WorkplaceHolder in the conveyor, where the given type of Workplace is
     * needed (needed for the current conveyor Product processing), it adds it
     * in there. If the conveyor is full or the given Workplace is not needed
     * for the processing, it does not add it. Added Workplace is set as added
     * by Workplace.addToConveyor(this).
     *
     * @param workplace Workplace to be added
     * @return true if the workplace was added, false otherwise
     */
    private boolean addWorkplace(Workplace workplace) {
        // cannot add an already placed workplace
        if (workplace.isInConveyor()) {
            return false;
        }
        for (int i = 0; i < totalHolders; ++i) {
            // if there is an empty holder, where the given workplace is needed
            if (holders[i] == null && productType.getConveyorWorkplaces()[i] == workplace.getWorkplaceType()) {
                workplace.addToConveyor(this);
                WorkplaceHolder wh = new WorkplaceHolder(workplace, this);
                holders[i] = wh;
                return true;
            }
        }
        return false;
    }

    private boolean prepare() {
        if (decomposed) {
            LOG.log(Level.WARNING, "Using {0} after decomposing", this);
            return false;
        }
        boolean ret = true;
        for (int i = 0; i < totalHolders; ++i) {
            if (holders[i] == null) {
                // should never happen
                LOG.log(Level.WARNING, "{0} misses a Workplaces", this);
                ret = false;
                continue;
            }
            Workplace workplace = holders[i].getWorkplace();
            if (!workplace.isReady()) {
                if (workplace.isMachine() && !((Machine)workplace).hasWorker()) {
                    eventChannel.addTask(new StockGetWorkerTask(this, currentContract.getPriority(), StockGetWorkerTask.WorkerIdentification.FREE));
                    if (response instanceof StockGetWorkerResponse) {
                        Worker worker = ((StockGetWorkerResponse)response).getWorkers().get(0);
                        if (worker != null) {
                            ((Machine)workplace).attachWorker(worker);
                            LOG.log(Level.FINEST, "A free worker added into the machine {0}", workplace);
                        }
                        else {
                            LOG.log(Level.FINEST, "No free worker for the machine {0} found", workplace);
                        }
                    }
                    else {
                        LOG.log(Level.WARNING, "{0} did not received a StockGetWorkerResponse", this);
                    }
                    response = null;
                }
                LOG.log(Level.FINE, "{0} in conveyor for {1} is not ready", new Object[]{workplace, productType.toString()});
                ret = false;
            }
        }
        return ret;
    }

    private void connectHolders() {
        for (int i = 0; i < totalHolders - 1; ++i) {
            holders[i].setNext(holders[i + 1]);
        }
    }

    private void clearWorkplaces() {
        for (WorkplaceHolder wh : holders) {
            if (wh == null) {
                continue;
            }
            Product product = wh.getWorkplace().getProcessedProduct();
            if (product != null) {
                // adds steal back into the stock
                eventChannel.addTask(new StockResourceTask(this, currentContract.getPriority(), ResourceType.STEAL, StockResourceTask.Action.ADD, product.getType().getStealNeeded()));
                --productsInProcess;
            }
        }
    }
}
