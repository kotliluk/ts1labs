
package cz.cvut.k36.omo.factory.contract;

/**
 * Enum declaring 3 levels of priority of Contracts and Events in the Factory:
 * LOW, NORMAL, HIGH. Value NO is auxiliary.
 */
public enum Priority {
    HIGH(3),
    NORMAL(2),
    LOW(1),
    NO(0);
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final int value;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    private Priority(int value) {
        this.value = value;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    /**
     * Checks whether this.getIntValue() is less than p2.getIntValue().
     * 
     * @param p2 other priority
     * @return true if less, false otherwise
     */
    public boolean isLess(Priority p2) {
        return value < p2.value;
    }

    /**
     * Checks whether this.getIntValue() is higher than p2.getIntValue().
     * 
     * @param p2 other priority
     * @return true if higher, false otherwise
     */
    public boolean isHigher(Priority p2) {
        return value > p2.value;
    }

    /**
     * Checks whether this.getIntValue() is same as p2.getIntValue().
     * 
     * @param p2 other priority
     * @return true if same, false otherwise
     */
    public boolean isSame(Priority p2) {
        return value == p2.value;
    }

    /**
     * Returns integer value of the priority (higher priorities has higher int
     * values).
     * 
     * @return int value
     */
    public int getIntValue() {
        return value;
    }
}
