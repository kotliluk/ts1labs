/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.events.StockResourceTask.Action;

/**
 * Response to a StockResourceTask holding a response amount of resource.
 */
public class StockResourceResponse extends Response {

    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final int responseAmount;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public StockResourceResponse(EventChannelAccessible sender, StockResourceTask previousTask, int responseAmount) {
        super(sender, previousTask);
        this.responseAmount = responseAmount;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public int getResponseAmount() {
        return responseAmount;
    }
    
    public Action getActionType() {
        return ((StockResourceTask)previousTask).getActionType();
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " returning " + responseAmount + " of " + ((StockResourceTask)previousTask).getResourceType()
                    + " from " + sender + " to " + previousTask.sender;
    }
}
