
package cz.cvut.k36.omo.factory.production;

import cz.cvut.k36.omo.factory.product.Product;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Holder of the Workplace in the Conveyor. Makes Chain of responsibility.
 * Design pattern: Chain of responsibility.
 */
class WorkplaceHolder {

    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static final Logger LOG = Logger.getLogger(WorkplaceHolder.class.getName());

    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final Workplace workplace;
    private WorkplaceHolder next;
    private final Conveyor conveyor;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    WorkplaceHolder(Workplace workplace, Conveyor conveyor) {
        this.workplace = workplace;
        this.next = null;
        this.conveyor = conveyor;
    }

    /* ----------------------- PACKAGE METHODS ----------------------- */
    
    /**
     * Passes given Product to the Workplace, which is in the WorkplaceHolder,
     * and gets previous Product from it. Previous Product is passed to the next
     * WorkplaceHolder in the chain, if it is not done yet.
     *
     * @param nextProduct Next Product to be processed in the place.
     * @return The finished Product, if any was finished in this Chain of
     * responsiility this tact, null otherwise.
     */
    Product runWorkplace(Product nextProduct) {
        Product curProduct = workplace.getProcessedProduct();
        workplace.processProduct(nextProduct);
        if (curProduct != null && curProduct.isDone()) {
            LOG.log(Level.FINE, "{0} created!", curProduct);
            return curProduct;
        } else {
            return passToNextWorkplace(curProduct);
        }
    }

    Workplace getWorkplace() {
        return workplace;
    }

    void setNext(WorkplaceHolder next) {
        this.next = next;
    }

    /* ----------------------- PRIVATE METHODS ----------------------- */
    
    private Product passToNextWorkplace(Product product) {
        if (next == null) {
            if (product != null) {
                LOG.log(Level.WARNING, "Product {0} in the last WorkplaceHolder is still not done", product);
            }
            return null;
        }
        return next.runWorkplace(product);
    }
}
