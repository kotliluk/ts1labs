
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.factory.Factory;
import cz.cvut.k36.omo.factory.contract.ConflictSolution;
import cz.cvut.k36.omo.factory.contract.Contract;

import java.util.List;

/**
 * Task for a factory containing list of possible solutions for a conflict caused by a Contract, which
 * is not possible to be processed immendiately. Used to inform the user.
 */
public class ContractConflictTask extends Task {

    /* ----------------------- INSTANCES' FIELDS ----------------------- */

    private final List<ConflictSolution> solutions;
    private final Contract contract;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    public ContractConflictTask(EventChannelAccessible sender, Contract contract, List<ConflictSolution> solutions) {
        super(contract.getPriority(), sender, Factory.class);
        this.contract = contract;
        this.solutions = solutions;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    /**
     * @return Contract which caused the conflict
     */
    public Contract getContract() {
        return contract;
    }

    /**
     * @return List of all found solutions to choose from
     */
    public List<ConflictSolution> getSolutions() {
        return solutions;
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " caused by " + contract;
    }
}
