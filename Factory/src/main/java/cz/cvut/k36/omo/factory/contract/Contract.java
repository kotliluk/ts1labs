
package cz.cvut.k36.omo.factory.contract;

import cz.cvut.k36.omo.factory.product.Product;
import cz.cvut.k36.omo.factory.product.ProductType;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Description of the contract to be done by the factory.
 */
public class Contract {

    /* ----------------------- TESTING STATIC FIELDS ----------------------- */

    private static final List<Contract> contracts = new LinkedList<>();

    public static void resetTestingStats() {
        contracts.clear();
    }

    public static List<Contract> getContracts() {
        return contracts;
    }

    public static int getContractsCount() {
        return contracts.size();
    }

    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static final Logger LOG = Logger.getLogger(Contract.class.getName());
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final Priority priority;
    private final ProductType productType;
    private final int orderedCount;
    private final int productSellingPrice;
    private final List<Product> products;
    private int finishedCount = 0;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public Contract(Priority priority, ProductType productType, int productSellingPrice, int orderedCount) {
        this.priority = priority;
        this.productType = productType;
        this.orderedCount = orderedCount;
        this.productSellingPrice = productSellingPrice;
        this.products = new LinkedList<>();
        // for testing
        contracts.add(this);
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public Priority getPriority() {
        return priority;
    }

    public ProductType getOrderedType() {
        return productType;
    }

    public int getOrderedCount() {
        return orderedCount;
    }

    public int getProductSellingPrice() {
        return productSellingPrice;
    }

    public int getFinishedCount() {
        return finishedCount;
    }

    public List<Product> getProducts() { return products; }
    
    /**
     * @return True if all ordered Products were added
     */
    public boolean isFinished() {
        return orderedCount == finishedCount;
    }
    
    /**
     * Adds given Product if the Contract is not finished yet and if the given
     * Product is done (product.isDone() returns true - it was fully created)
     * and if the given Product is of the same type as the Contract and if the
     * Product was not added to any Contract before (product.isSold() returns
     * false). Added product is set as sold, so from now on calling
     * product.isSold() returns false.
     * 
     * @param product Product to be added
     * @return True if the given Product was added, false otherwise
     */
    public boolean addFinishedProduct(Product product) {
        if (!isFinished() && product.isDone()
                && product.getType() == productType && !product.isSold()) {
            product.setSold();
            products.add(product);
            ++finishedCount;
            LOG.log(Level.FINE, "{0} received a new finished Product: {1}", new Object[]{this, product});
            return true;
        }
        return false;
    }
    
    @Override
    public String toString() {
        return "Contract of priority " + priority.name() + " for " + orderedCount + " " + productType + "s";
    }
}
