
package cz.cvut.k36.omo.factory.contract;

import cz.cvut.k36.omo.factory.production.Conveyor;
import cz.cvut.k36.omo.factory.production.WorkplaceType;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Description of one solution of the situation, when there is not currently
 * enough Workplaces available to process a newly added Contract.
 * Contains an inner enumeration ConflictSolutionType to describe the type of
 * the solution.
 */
public class ConflictSolution {
    
    public enum ConflictSolutionType {
        POSTPONE_CONTRACT("Postpone the contract"),
        REPLACE_CONTRACT("Replace the contract"),
        DECOMPOSE_CONVEYOR("Decompose the conveyor"),
        BUY_WORKPLACES("Buy new needed workplaces");
        
        private final String string;
        
        private ConflictSolutionType(String string) {
            this.string = string;
        }
        
        @Override
        public String toString() {
            return string;
        }
    }
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final ConflictSolutionType type;
    private final List<WorkplaceType> workplaces;
    private final int cost;
    private final Conveyor conveyor;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    /**
     * Creates a POSTPONE_CONTRACT type solution, which is always the same.
     */
    public ConflictSolution() {
        this.type = ConflictSolutionType.POSTPONE_CONTRACT;
        this.workplaces = null;
        this.cost = 0;
        this.conveyor = null;
    }
    
    /**
     * Creates a BUY_WORKPLACES type solution.
     * 
     * @param workplaces Workplaces which are needed to be bought
     * @param cost Workplaces' cost
     */
    public ConflictSolution(List<WorkplaceType> workplaces, int cost) {
        this.type = ConflictSolutionType.BUY_WORKPLACES;
        this.workplaces = workplaces;
        this.cost = cost;
        this.conveyor = null;
    }
    
    /**
     * Creates a BUY_WORKPLACES type solution.
     * 
     * @param workplaces Workplaces which are needed to be bought
     * @param cost Workplaces' cost
     */
    public ConflictSolution(Map<WorkplaceType, Integer> workplaces, int cost) {
        this.type = ConflictSolutionType.BUY_WORKPLACES;
        this.workplaces = new LinkedList<>();
        for (WorkplaceType wt : workplaces.keySet()) {
            for (int i = 0; i < workplaces.get(wt); ++i) {
                this.workplaces.add(wt);
            }
        }
        this.cost = cost;
        this.conveyor = null;
    }
    
    /**
     * Creates a DECOMPOSE_CONVEYOR or REPLACE_CONTRACT type solution.
     * 
     * @param conveyor Conveyor to be decomposed or which the Contract is meant
     * to be replaced in
     * @param type DECOMPOSE_CONVEYOR or REPLACE_CONTRACT to set the action with
     * the conveyor     
     */
    public ConflictSolution(Conveyor conveyor, ConflictSolutionType type) throws IllegalArgumentException {
        if (type != ConflictSolutionType.DECOMPOSE_CONVEYOR && type != ConflictSolutionType.REPLACE_CONTRACT) {
            throw new IllegalArgumentException();
        }
        this.type = type;
        this.workplaces = null;
        this.cost = 0;
        this.conveyor = conveyor;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public ConflictSolutionType getType() {
        return type;
    }

    public List<WorkplaceType> getWorkplaces() {
        return workplaces;
    }

    public int getCost() {
        return cost;
    }

    public Conveyor getConveyor() {
        return conveyor;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(type.toString());
        switch (type) {
            case BUY_WORKPLACES:
                sb.append(": ");
                for (WorkplaceType wt : workplaces) {
                    sb.append(wt.toString()).append(", ");
                }
                sb.append("in total cost: ").append(cost);
                break;
            case DECOMPOSE_CONVEYOR:
                sb.append(conveyor.toString());
                break;
            case REPLACE_CONTRACT:
                sb.append(" in ").append(conveyor.toString());
                break;
        }
        return sb.toString();
    }
}
