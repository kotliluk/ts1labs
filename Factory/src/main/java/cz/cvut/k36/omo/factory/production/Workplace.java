/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.k36.omo.factory.production;

import cz.cvut.k36.omo.factory.archive.Archive;
import cz.cvut.k36.omo.factory.archive.ArchiveType;
import cz.cvut.k36.omo.factory.archive.ConsumptionArchive;
import cz.cvut.k36.omo.factory.product.Product;
import cz.cvut.k36.omo.factory.supply.ResourceType;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Workplace processing a Product. Needs to be placed in a WorkplaceHolder for
 * a Product to be passed by chain of responsibility.
 * 
 * Design pattern: Template method
 */
public abstract class Workplace {
    
    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static int workplaceCount = 0;
    private static final Logger LOG = Logger.getLogger(Workplace.class.getName());
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    protected final int workplaceID;
    protected final WorkplaceType workplaceType;
    protected Product curProduct = null;
    protected Conveyor curConveyor = null;
    protected ConsumptionArchive consumptionArchive;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public Workplace(WorkplaceType workplaceType) {
        this.workplaceID = ++workplaceCount;
        this.workplaceType = workplaceType;
        consumptionArchive = (ConsumptionArchive)(Archive.getInstance().getSpecialArchive(ArchiveType.CONSUMPTION));
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public final Conveyor getConveyor() {
        return curConveyor;
    }
    
    public final boolean isInConveyor() {
        return curConveyor != null;
    }
    
    public final WorkplaceType getWorkplaceType() {
        return workplaceType;
    }
    
    public final int getWorkplaceID() {
        return workplaceID;
    }
    
    /* ----------------------- PACKAGE METHODS ----------------------- */
    
    abstract boolean isReady();
    
    abstract boolean isMachine();
    
    abstract boolean isRobot();
    
    /**
     * Processes given Product.
     * 
     * @param product Product to be processed
     */
    final void processProduct(Product product) {
        if (product == null) {
            return;
        }
        if (!useResources()) {
            LOG.log(Level.WARNING, "{0} did not get enough needed resources", this);
        }
        curProduct = product;
        LOG.log(Level.FINE, "{0} processes {1}", new Object[]{workplaceType, curProduct});
        product.beProcessedBy(this);
        specialProcessFeature();
    }
    
    /**
     * @return Last processes Product in the Workplace
     */
    final Product getProcessedProduct() {
        Product ret = curProduct;
        curProduct = null;
        return ret;
    }
    
    /**
     * Adds the Workplace into the Conveyor. If null is given as a conveyor,
     * Workplace is now free (isInConveyor() will return false).
     * 
     * @param conveyor New parent Conveyor
     */
    final void addToConveyor(Conveyor conveyor) {
        this.curConveyor = conveyor;
    }

    /* ----------------------- PROTECTED METHODS ----------------------- */
    
    /**
     * Adds special funcionality to the processing of the Product by child classes.
     */
    protected abstract void specialProcessFeature();

    /* ----------------------- PRIVATE METHODS ----------------------- */
    
    private boolean useResources() {
        int electricityNeeded = workplaceType.getElectricityConsumption();
        if (electricityNeeded > 0) {
            LOG.log(Level.FINEST, "{0} needs {1} electricity", new Object[]{this, electricityNeeded});
            int electricity = curConveyor.useResources(ResourceType.ELECTRICITY, electricityNeeded);
            consumptionArchive.addLogInArchive(this, ResourceType.ELECTRICITY, electricity);
            if (electricity < electricityNeeded) {
                return false;
            }
        }
        int oilNeeded = workplaceType.getOilConsumption();
        if (oilNeeded > 0) {
            LOG.log(Level.FINEST, "{0} needs {1} oil", new Object[]{this, oilNeeded});
            int oil = curConveyor.useResources(ResourceType.OIL, oilNeeded);
            consumptionArchive.addLogInArchive(this, ResourceType.OIL, oil);
            if (oil < oilNeeded) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public String toString() {
        return workplaceType + " with ID " + workplaceID;
    }
}
