
package cz.cvut.k36.omo.factory.production;

import cz.cvut.k36.omo.factory.worker.Worker;

/**
 * Machine processing a Product. Needs to be placed in a WorkplaceHolder for
 * a Product to be passed by chain of responsibility. Needs a Worker to be able
 * to work.
 */
public class Machine extends Workplace {
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private Worker worker;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public Machine(MachineType machineType) {
        super(machineType);
    }
    
    /* ----------------------- PACKAGE METHODS ----------------------- */
    
    /**
     * @return False if the Machine misses a Worker, true otherwise
     * when it is ready to process
     */
    @Override
    boolean isReady() {
        return hasWorker();
    }
    
    @Override
    boolean isMachine() {
        return true;
    }
    
    @Override
    boolean isRobot() {
        return false;
    }
    
    boolean hasWorker() {
        return this.worker != null;
    }
    
    /**
     * Tries to attach the given Worker to the Machine. If there already is
     * a Worker in the Machine, does nothing and returns false. If there was
     * no Worker before so the new one is attached, he is set as attached to
     * this Machine by calling worker.beAttachedTo(this).
     * 
     * @param worker Worker to be attached
     * @return True if new Worker was added, false otherwise
     */
    boolean attachWorker(Worker worker) {
        if (this.worker == null) {
            this.worker = worker;
            worker.beAttachedTo(this);
            return true;
        }
        return false;
    }
    
    /**
     * Detaches the Worker from the Machine if there is any.
     * 
     * @return The Worker if there was a Worker who was detached, null if there
     * was no Worker
     */
    Worker detachWorker() {
        if (this.worker == null) {
            return null;
        }
        Worker ret = this.worker;
        this.worker.beAttachedTo(null);
        this.worker = null;
        return ret;
    } 

    /* ----------------------- PROTECTED METHODS ----------------------- */
    
    @Override
    protected void specialProcessFeature() {
        this.worker.nextTactOfWorkDone();
    }
}
