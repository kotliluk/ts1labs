
package cz.cvut.k36.omo.factory;

import cz.cvut.k36.omo.factory.factory.Factory;
import cz.cvut.k36.omo.factory.factory.FactoryBuilder;
import cz.cvut.k36.omo.factory.factory.FactoryUIStandardInOut;
import cz.cvut.k36.omo.factory.factory.FactoryUIWrapper;
import cz.cvut.k36.omo.factory.supply.ResourceType;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 */
public class Main {
    
    public static void main(String[] args) {
        Logger root = Logger.getLogger("");
        root.getHandlers()[0].setLevel(Level.WARNING);  
        root.setLevel(Level.WARNING);
        
//        Factory f = FactoryBuilder.createDefaultFactory().getFactory();
        Factory f = FactoryBuilder.createEmptyFactory().getFactory();
        //Factory f = FactoryBuilder.createDefaultFactory().getFactory();
//        Factory f = FactoryBuilder.createEmptyFactory().addResource(ResourceType.MONEY, 100000).getFactory();
        FactoryUIStandardInOut ui = new FactoryUIStandardInOut(f);
        ui.run();
        //ui.testRun(List.of("help"));
        //ui.testRun(List.of("help"));
    }
}
