
package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.contract.Priority;
import cz.cvut.k36.omo.factory.supply.Stock;
import cz.cvut.k36.omo.factory.production.Workplace;

/**
 * Task for a Stock to add a given Workplace.
 */
public class StockAddWorkplaceTask extends Task {

    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final Workplace workplace;
    
    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public StockAddWorkplaceTask(EventChannelAccessible sender, Priority priority, Workplace workplace) {
        super(priority, sender, Stock.class);
        this.workplace = workplace;
    }
    
    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    public Workplace getWorkplace() {
        return workplace;
    }
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " from " + sender + " to add Workplace " + workplace;
    }
}
