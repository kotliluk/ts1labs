
package cz.cvut.k36.omo.factory.events;

/**
 * Observer in a EventChannel conveying Tasks and Responses to
 * a EventChannelAccessible owner of the Observer.
 */
public class Observer {
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */
    
    private final EventChannelAccessible owner;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    /**
     * @param owner – task recipient
     */
    public Observer(EventChannelAccessible owner){
        this.owner = owner;
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    /**
     * Through this method, the listener will receive the task
     * @param task Task to be given to the owner
     */
    public void update(Task task){
        owner.processTask(task);
    }

    /**
     * Through this method, the listener will receive the response
     * @param response response to be given to the owner
     */
    public void update(Response response){
        owner.receiveResponse(response);
    }

    public EventChannelAccessible getOwner() {
        return owner;
    }

    @Override
    public String toString() {
        return "Observer of " + owner;
    }
}

