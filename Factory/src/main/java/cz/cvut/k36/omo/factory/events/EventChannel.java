package cz.cvut.k36.omo.factory.events;

import cz.cvut.k36.omo.factory.archive.Archive;
import cz.cvut.k36.omo.factory.archive.ArchiveType;
import cz.cvut.k36.omo.factory.archive.EventArchive;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Conveys Tasks and Responses between EventChannelAccessible implementing classes.
 */
public class EventChannel {
    
    /* ----------------------- STATIC FIELDS ----------------------- */
    
    private static final Logger LOG = Logger.getLogger(EventChannel.class.getName());
    
    /* ----------------------- INSTANCES' FIELDS ----------------------- */

    private final List<Observer> observers;
    private final EventArchive archive;

    /* ----------------------- CONSTRUCTORS ----------------------- */
    
    public EventChannel(){
        observers = new LinkedList<>();
        archive = (EventArchive) Archive.getInstance().getSpecialArchive(ArchiveType.EVENT);
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    /**
     * @param task - task for execution
     */
    public void addTask(Task task) {
        LOG.log(Level.FINE, "New task in EventChannel: {0}", task);
        archive.addHistoryTask(task);
        notifyObservers(task);
    }

    public void addResponse(Response response) {
        LOG.log(Level.FINE, "New response in EventChannel: {0}", response);
        archive.addHistoryResponse(response);
        notifyObservers(response);
    }

    /**
     * Adding a listener to the list
     * @param observerOwner - new listening member in eventChannel to attach
     */
    public void attach(EventChannelAccessible observerOwner){
        // to not add twice
        for (Observer observer : observers) {
            if (observer.getOwner() == observerOwner) {
                return;
            }
        }
        observers.add(new Observer(observerOwner));
    }

    /**
     * Deleting a listener from the list
     * @param observerOwner - listening member in eventChannel to detach
     */
    public void detach(EventChannelAccessible observerOwner){
        Observer toDelete = null;
        for (Observer observer : observers) {
            if (observer.getOwner() == observerOwner) {
                toDelete = observer;
                break;
            }
        }
        if (toDelete != null) {
            observers.remove(toDelete);
        }
    }

    /* ----------------------- PUBLIC METHODS ----------------------- */
    
    /**
     * Notify all interested listeners of a task
     * @param task
     */
    private void notifyObservers(Task task){
        List<Observer> interested = observers.stream()
                .filter(o -> o.getOwner().getClass() == task.getRecipient())
                .collect(Collectors.toList());
        interested.stream().forEach((o) -> {
            LOG.log(Level.FINEST, "Notifying {0}", o);
            o.update(task);
        });
    }

    /**
     * Notify the interested listener of a response
     * @param response
     */
    private void notifyObservers(Response response){
        Observer interested = observers.stream()
                .filter(o -> o.getOwner() == response.getRecipient())
                .findAny()
                .get();
        if (interested != null) {
            LOG.log(Level.FINEST, "Notifying {0}", interested);
            interested.update(response);
        }
    }
}
