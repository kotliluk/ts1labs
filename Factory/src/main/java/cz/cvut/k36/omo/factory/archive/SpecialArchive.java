package cz.cvut.k36.omo.factory.archive;

public interface SpecialArchive {
    ArchiveType getType();
}
