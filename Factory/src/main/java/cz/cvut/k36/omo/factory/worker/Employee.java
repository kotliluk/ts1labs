
package cz.cvut.k36.omo.factory.worker;

/**
 * Employee interface for all employees in a factory who receives a bill for
 * their job.
 */
public interface Employee {
    
    /**
     * Increases number of total hours which the Worker worker and number of
     * hours which the Worker worked since last billing by one.
     */
    public void nextTactOfWorkDone();
    
    /**
     * @return Number of hours which the Worker worked from last billing
     */
    public int getHours();
    
    /**
     * @return Number of total hours which the Worker worked
     */
    public int getHoursInTotal();
    
    /**
     * Returns number of hours which the Worker worked from last billing and sets
     * them to zero. Worked hours got from this calling must be used to compute
     * Worker's bill.
     * 
     * @return Number of hours which the Worker worked from last billing
     */
    public int getHoursToBilling();
    
    /**
     * Receives given positive amount of money.
     * 
     * @param money employee's wage
     */
    public void earnMoney(int money);
}
