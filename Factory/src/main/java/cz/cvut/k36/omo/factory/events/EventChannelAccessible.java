package cz.cvut.k36.omo.factory.events;

/**
 * User of the EventChannel. To be notified, a Observer needs to added into
 * EventChannel by calling EventChannel::attach method.
 */
public interface EventChannelAccessible {
    
    /**
     * Reacts to the received Task.
     * 
     * @param task Task identified for this instance
     */
    void processTask(Task task);
    
    /**
     * Receives a Response.
     * 
     * @param response Received response
     */
    void receiveResponse(Response response);
}
