package cz.cvut.fel.ts1;

public class Kotliluk {

    public long factorial(int n) {
        long result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}
