package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class KotlilukTest {

    Kotliluk instance = new Kotliluk();

    @Test
    public void factorialTest() {
        long expected = 120;
        int input = 5;

        assertEquals(expected, instance.factorial(input));
    }
}